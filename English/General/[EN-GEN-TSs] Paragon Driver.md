### [EN-GEN-TSs] Paragon Driver

Overview:

Paragon Driver is a software for Windows and macOS to serve as a driver to read and write macOS extended in Windows and NTFS in macOS. The template is focused in macOS but contains the necessary resources to be used for Windows too. This is used when is necessary to use a drive between the two operating systems.

---

Preview:

<p>Keep in mind that the drive comes formatted in NTFS, which is a file system for Microsoft Windows. To get full NTFS compatibility with macOS it is recommended to download and install the Paragon Driver for macOS.<br />In the other way, to get full compatibility of MacOS Extended in Windows is recommended to download and install the Paragon Driver for Windows.</p>
<ul>
<li>To read and write NTFS in macOS, install the <a href="https://www.seagate.com/files/www-content/support-content/external-products/backup-plus/_shared/downloads/NTFS_for_Mac.dmg">Paragon Driver for macOS</a>.</li>
<li>To read and write MacOS Extended in Windows, install the <a href="https://www.seagate.com/files/www-content/support-content/external-products/backup-plus/_shared/downloads/HFS4WIN.msi">Paragon Driver for Windows</a>.</li>
<li><a href="https://www.seagate.com/support/software/paragon/">Paragon Driver documentation</a>.</li>
</ul>

---

Plain text:

```
Keep in mind that the drive comes formatted in NTFS, which is a file system for Microsoft Windows. To get full NTFS compatibility with macOS it is recommended to download and install the Paragon Driver for macOS.
In the other way, to get full compatibility of MacOS Extended in Windows is recommended to download and install the Paragon Driver for Windows.

To read and write NTFS in macOS, install the Paragon Driver for macOS:
https://www.seagate.com/files/www-content/support-content/external-products/backup-plus/_shared/downloads/NTFS_for_Mac.dmg.
To read and write MacOS Extended in Windows, install the Paragon Driver for Windows:
https://www.seagate.com/files/www-content/support-content/external-products/backup-plus/_shared/downloads/HFS4WIN.msi.
Paragon Driver documentation:
https://www.seagate.com/support/software/paragon/.
```
---

HTML code:

```
<p>Keep in mind that the drive comes formatted in NTFS, which is a file system for Microsoft Windows. To get full NTFS compatibility with macOS it is recommended to download and install the Paragon Driver for macOS.<br />In the other way, to get full compatibility of MacOS Extended in Windows is recommended to download and install the Paragon Driver for Windows.</p>
<ul>
<li>To read and write NTFS in macOS, install the <a href="https://www.seagate.com/files/www-content/support-content/external-products/backup-plus/_shared/downloads/NTFS_for_Mac.dmg">Paragon Driver for macOS</a>.</li>
<li>To read and write MacOS Extended in Windows, install the <a href="https://www.seagate.com/files/www-content/support-content/external-products/backup-plus/_shared/downloads/HFS4WIN.msi">Paragon Driver for Windows</a>.</li>
<li><a href="https://www.seagate.com/support/software/paragon/">Paragon Driver documentation</a>.</li>
</ul>
```
---

Speed note:

Recommended to install Paragon Driver, included download link and support page.
