### [EN-GEN-CSs] Adobe Promo Link

Overview:

When the promo for Adobe Creative Cloud is claimed, applies also to Mylio. Template is universal, applies to Seagate and LaCie.

---

Preview:

<p>Hello {!Contact.FirstName} {!Contact.LastName}</p>
<p>Thank you for contacting Seagate Support, my name is {!Case.OwnerFirstName} and it is my pleasure to assist you today with the redeeming of Adobe Promo attached to your device. Please trust that together we will find the best solution to your case.</p>
<p>Here is the link for redeeming the Adobe Creative Cloud Photography:<br />\##linkpro##</p>
<p>Please reply to this email if you require further assistance.</p>
<p>Regards,<br />{!Case.OwnerFirstName}<br />Seagate Support</p>

---

Plain text:

```
Hello {!Contact.FirstName} {!Contact.LastName}

Thank you for contacting Seagate Support, my name is {!Case.OwnerFirstName} and it is my pleasure to assist you today with the redeeming of Adobe Promo attached to your device. Please trust that together we will find the best solution to your case.

Here is the link for redeeming the Adobe Creative Cloud Photography:
\##linkpro##

Please reply to this email if you require further assistance.

Regards,
{!Case.OwnerFirstName}
Seagate Support
```
---

HTML code:

```
<p>Hello {!Contact.FirstName} {!Contact.LastName}</p>
<p>Thank you for contacting Seagate Support, my name is {!Case.OwnerFirstName} and it is my pleasure to assist you today with the redeeming of Adobe Promo attached to your device. Please trust that together we will find the best solution to your case.</p>
<p>Here is the link for redeeming the Adobe Creative Cloud Photography:<br />\##linkpro##</p>
<p>Please reply to this email if you require further assistance.</p>
<p>Regards,<br />{!Case.OwnerFirstName}<br />Seagate Support</p>
```
---

Speed note:

2. Emailed to Hultman requesting a promo code based in the number case, serial number and customer email. Received code a few minutes later.
3. Composed an email to the customer providing the link and inviting to reply the email if more assistance is required.
