### [EN-GEN-CSs] Serial number request

Overview:

When serial number is not specified, this template explains the importance of serial number and provides links to help to find the serial number in Seagate and LaCie devices.

---

Preview:

<p>First, in order for us to provide the most accurate support for your device, we need the device serial number. This is to take particularities related to your drive into account as well as to verify the warranty information of the drive. The serial number is a combination of eight letters and numbers found on the bottom of the drive, it is typically presented as &ldquo;SN&rdquo; or &ldquo;S/N: XXXXXXXX&rdquo;. <br />Check this support page for how to find the serial number for <a href="http://www.seagate.com/support/warranty-and-replacements/find-model-number/">Seagate</a> and <a href="https://www.lacie.com/support/kb/instructions-to-find-a-serial-number-on-the-lacie-drives-007763en/">LaCie</a> devices.<br /><br />Otherwise, you can also reply to this email with pictures of all the sides of the device to find the label that contains the serial number.</p>

---

Plain text:

```
First, in order for us to provide the most accurate support for your device, we need the device serial number. This is to take particularities related to your drive into account as well as to verify the warranty information of the drive. The serial number is a combination of eight letters and numbers found on the bottom of the drive, it is typically presented as “SN” or “S/N: XXXXXXXX”. Check this support page for how to find the serial number:
http://www.seagate.com/support/warranty-and-replacements/find-model-number/
https://www.lacie.com/support/kb/instructions-to-find-a-serial-number-on-the-lacie-drives-007763en/.
Otherwise, you can also reply to this email with pictures of all the sides of the device to find the label that contains the serial number.
```
---

HTML code:

```
<p>First, in order for us to provide the most accurate support for your device, we need the device serial number. This is to take particularities related to your drive into account as well as to verify the warranty information of the drive. The serial number is a combination of eight letters and numbers found on the bottom of the drive, it is typically presented as &ldquo;SN&rdquo; or &ldquo;S/N: XXXXXXXX&rdquo;. <br />Check this support page for how to find the serial number for <a href="http://www.seagate.com/support/warranty-and-replacements/find-model-number/">Seagate</a> and <a href="https://www.lacie.com/support/kb/instructions-to-find-a-serial-number-on-the-lacie-drives-007763en/">LaCie</a> devices.<br /><br />Otherwise, you can also reply to this email with pictures of all the sides of the device to find the label that contains the serial number.</p>
```
---

Speed note:

Requested the serial number of device, included help links to Seagate and LaCie.
