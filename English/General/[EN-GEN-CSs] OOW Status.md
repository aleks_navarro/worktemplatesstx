### [EN-GEN-CSs] OOW Status

Overview:

Use this template for a warranty review as Out of warranty, requesting POP and including a link for help.

---

Preview:

<p>By reviewing in our system the hard drive with serial number {!Case.Serial_Number__c} it is out of warranty since {!Case.Warranty_End_Date__c}. If this information is wrong or there are doubts regarding this warranty status, then please reply to this email attaching the invoice note or proof of purchase of the drive, and we will analyze the warranty situation of the drive.<br />For more information about OOW products please check <a href="https://www.seagate.com/support/kb/why-is-my-drive-out-of-warranty-192831en/">this support page</a>.</p>

---

Plain text:

```
By reviewing in our system the hard drive with serial number {!Case.Serial_Number__c} it is out of warranty since {!Case.Warranty_End_Date__c}. If this information is wrong or there are doubts regarding this warranty status, then please reply to this email attaching the invoice note or proof of purchase of the drive, and we will analyze the warranty situation of the drive.
For more information about OOW products please check the following support page
https://www.seagate.com/support/kb/why-is-my-drive-out-of-warranty-192831en/.
```
---

HTML code:

```
<p>By reviewing in our system the hard drive with serial number {!Case.Serial_Number__c} it is out of warranty since {!Case.Warranty_End_Date__c}. If this information is wrong or there are doubts regarding this warranty status, then please reply to this email attaching the invoice note or proof of purchase of the drive, and we will analyze the warranty situation of the drive.<br />For more information about OOW products please check <a href="https://www.seagate.com/support/kb/why-is-my-drive-out-of-warranty-192831en/">this support page</a>.</p>
```
---

Speed note:

Review and explained the current warranty status as OOW, requesting POP if wrong and including help link.
