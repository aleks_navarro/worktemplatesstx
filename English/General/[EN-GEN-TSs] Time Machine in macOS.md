### [EN-GEN-TSs] Time Machine in macOS

Overview:

A template including instructions to set up a backup in Time Machine, including a link to Apple Support.

---

Preview:

<p>It is possible to set up a backup in Mac using Time Machine, which is an incorporated software to macOS. Follow the next steps to set up a backup with Time Machine:</p>
<ol>
<li>Connect your drive and Open Time Machine preferences from the Time Machine menu, Time Machine icon in the menu bar. Or choose Apple () menu &gt; System Preferences, then click Time Machine.</li>
<li>Click Select Backup Disk, Select Disk, or Add or Remove Backup Disk.</li>
<li>Select a backup disk from the list, then click Use Disk. Wait until the process is complete.</li>
<li>For additional backup security and convenience, you can repeat these steps to add another backup disk. For example, you might use one backup disk while at home and another backup disk while at work.</li>
<li>Eject your external drive and unplug it.</li>
</ol>
<p>If you have more doubts, please click here to find more info about <a href="https://support.apple.com/en-us/HT201250">Time Machine in Apple Support</a>.<br /><em>Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.</em></p>

---

Plain text:

```
It is possible to set up a backup in Mac using Time Machine, which is an incorporated software to macOS. Follow the next steps to set up a backup with Time Machine:
1. Connect your drive and Open Time Machine preferences from the Time Machine menu, Time Machine icon in the menu bar. Or choose Apple () menu > System Preferences, then click Time Machine.
2. Click Select Backup Disk, Select Disk, or Add or Remove Backup Disk.
3. Select a backup disk from the list, then click Use Disk.
4. Wait until the process is complete.
5. For additional backup security and convenience, you can repeat these steps to add another backup disk. For example, you might use one backup disk while at home and another backup disk while at work.
6. Eject your external drive and unplug it.

If you have more doubts, please click here to find more info about Time Machine in Apple Support:
https://support.apple.com/en-us/HT201250.
Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.
```
---

HTML code:

```
<p>It is possible to set up a backup in Mac using Time Machine, which is an incorporated software to macOS. Follow the next steps to set up a backup with Time Machine:</p>
<ol>
<li>Connect your drive and Open Time Machine preferences from the Time Machine menu, Time Machine icon in the menu bar. Or choose Apple () menu &gt; System Preferences, then click Time Machine.</li>
<li>Click Select Backup Disk, Select Disk, or Add or Remove Backup Disk.</li>
<li>Select a backup disk from the list, then click Use Disk. Wait until the process is complete.</li>
<li>For additional backup security and convenience, you can repeat these steps to add another backup disk. For example, you might use one backup disk while at home and another backup disk while at work.</li>
<li>Eject your external drive and unplug it.</li>
</ol>
<p>If you have more doubts, please click here to find more info about <a href="https://support.apple.com/en-us/HT201250">Time Machine in Apple Support</a>.<br /><em>Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.</em></p>
```
---

Speed note:

Including instructions to set up a backup with Time Machine in macOS, with link to Apple Support with third party url script.
