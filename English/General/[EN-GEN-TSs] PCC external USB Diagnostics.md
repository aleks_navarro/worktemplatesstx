### [EN-GEN-TSs] PCC external USB Diagnostics

Overview:

Link to External USB Diagnostics guide in Seagate. This is usually used to test drives that are connected via SATA to USB adapter. See also: PCC for external drives.

---

Preview:

<p>Try following the diagnostics guide for <a href="https://www.seagate.com/support/kb/seagate-usb-external-drive-diagnostics-006153en/">USB External drives in Seagate Support</a>. Please reply to this email with the results of the diagnostics.</p>

---

Plain text:

```
Try following the diagnostics guide for USB External drives in Seagate Support
https://www.seagate.com/support/kb/seagate-usb-external-drive-diagnostics-006153en/.
Please reply to this email with the results of the diagnostics.
```
---

HTML code:

```
<p>Try following the diagnostics guide for <a href="https://www.seagate.com/support/kb/seagate-usb-external-drive-diagnostics-006153en/">USB External drives in Seagate Support</a>. Please reply to this email with the results of the diagnostics.</p>
```
---

Speed note:

Included a link to Seagate USB External Drives Diagnostics.
