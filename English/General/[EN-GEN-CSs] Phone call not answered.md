### [EN-GEN-CSs] Phone call not answered

Overview:

Template included when a phone call was made and but not answered. Modify to specify if a voice message was left or not.

---

Preview:

I would like to notify you that phone call was made to the phone number provided in our system, {!Contact.Phone}; nobody answered, so a voice message was left.

---

Plain text:

```
I would like to notify you that phone call was made to the phone number provided in our system, {!Contact.Phone}; nobody answered, so a voice message was left.
```
---

HTML code:

```
<p>I would like to notify you that phone call was made to the phone number provided in our system, {!Contact.Phone}; nobody answered, so a voice message was left.</p>
```
---

Speed note:

Notified to the customer about the phone call and voice message.
