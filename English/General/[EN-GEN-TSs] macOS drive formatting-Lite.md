### [EN-GEN-TSs] macOS drive formatting-Lite

Overview:

Lighter template for macOS drive formatting template.

---

Preview:

<p>In this case is recommended to format the hard drive for macOS, follow the instructions for formatting in this <a href="https://www.seagate.com/support/kb/how-to-format-your-drive-in-macos-1011-and-above-007736en/">support page from Seagate</a>.</p>

---

Plain text:

```
In this case is recommended to format the hard drive for macOS, follow the instructions for formatting in this support page from Seagate:
https://www.seagate.com/support/kb/how-to-format-your-drive-in-macos-1011-and-above-007736en/.
```

---

HTML code:

```
<p>In this case is recommended to format the hard drive for macOS, follow the instructions for formatting in this <a href="https://www.seagate.com/support/kb/how-to-format-your-drive-in-macos-1011-and-above-007736en/">support page from Seagate</a>.</p>
```

---

Speed note:

Included link to Seagate support for instructions for formatting in macOS.
