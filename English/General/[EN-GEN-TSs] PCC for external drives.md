### [EN-GEN-TSs] PCC for external drives

Overview:
This template is designed to provide a basic troubleshooting guide to the customer to test the hard drive in order to find issues with the external drive. This includes a review of the current data status of the device, basic check of cable, power supply, port and computer, use of Disk Management in Windows and Disk Utility in macOS to check the status of the drive, link to formatting instructions, physical revision of disk and a link to advanced troubleshooting steps. This template is meaning to be universal to Seagate or LaCie (and even every external hard drive out there) and OS agnostic (obviating the no-support politics for Unix and GNU/Linux). This template can also work with DAS devices, but it may require to integrate some RAID questions when needed. It is not contemplating the drives that require external power supply, in case of needed refer to the templates regarding to power supply issues. See also: PCC for internal drives, PCC for NAS, Current Data Status, Power Supply, DIY Recovery, RMA.

---

Preview:

<p>We are going to try the following troubleshooting guide to determine if there is no problem with the drive itself. Please check and answer the following:</p>
<ul>
<li>Do you have a backup of the data in the drive or there is risk of data loss?</li>
<li>Was being used a software for backup management?</li>
<li>Try to use another USB cable, similar or equal, for connecting your drive.</li>
<li>Try to connect it to another port on your computer or another computer.</li>
<li>Check the current state of the drive in a computer:<br />
<ul>
<li>In Windows, check the hard drive status in Disk Management (Windows and X keys on the keyboard, select Disk Management).</li>
<li>In macOS, check the hard drive status in Disk Utility (Finder &gt; Applications &gt; Utilities &gt; Disk Utility).</li>
</ul>
</li>
<li>The drive may be corrupted. Reformatting your drive can solve this issue.<br />
<ul>
<li><a href="https://www.seagate.com/support/kb/how-to-format-your-hard-drive-220151en/">Instructions for formatting</a>.<br /><em>Please be aware that formatting will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.</em></li>
</ul>
</li>
<li>Is the LED light of the hard drive turning on? What is its behavior: It is always off, always on or does it blink?</li>
<li>Put the hand over the drive, does it feel a slight movement like spinning? Does it feel hard or doesn&rsquo;t feel at all?</li>
<li>If the drive is making strange noises, like clicking or buzzing, or have slower performance than the normal, please disconnect immediately the drive from the computer and reply to this email.</li>
<li>Follow this <a href="https://www.seagate.com/support/kb/seagate-usb-external-drive-diagnostics-006153en/">guide in Seagate for more advanced troubleshooting.</a></li>
</ul>

---

Plain text:

```
We are going to try the following troubleshooting guide to determine if there is no problem with the drive itself. Please check and answer the following:
Do you have a backup of the data in the drive or is there a data loss risk?
Was being used a software for backup management?
Try to use another USB cable, similar or equal, for connecting your drive.
Try to connect it to another port on your computer or another computer.
Check the current state of the drive in a computer:
In Windows, check the hard drive status in Disk Management (Windows and X keys on the keyboard, select Disk Management).
In macOS, check the hard drive status in Disk Utility (Finder > Applications > Utilities > Disk Utility).
The drive may be corrupted. Reformatting your drive can solve this issue.
Instructions for formatting
https://www.seagate.com/support/kb/how-to-format-your-hard-drive-220151en/.
Please be aware that formatting will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.
Is the LED light of the hard drive turning on? What is its behavior: It is always off, always on or does it blink?
Put the hand over the drive, does it feel a slight movement like spinning? Does it feel hard or doesn’t feel at all?
If the drive is making strange noises, like clicking or buzzing, or have slower performance than the normal, please disconnect immediately the drive from the computer and reply to this email.
Follow this guide in Seagate for more advanced troubleshooting:
https://www.seagate.com/support/kb/seagate-usb-external-drive-diagnostics-006153en/.
```
---

HTML code:

```
<p>We are going to try the following troubleshooting guide to determine if there is no problem with the drive itself. Please check and answer the following:</p>
<ul>
<li>Do you have a backup of the data in the drive or there is risk of data loss?</li>
<li>Was being used a software for backup management?</li>
<li>Try to use another USB cable, similar or equal, for connecting your drive.</li>
<li>Try to connect it to another port on your computer or another computer.</li>
<li>Check the current state of the drive in a computer:<br />
<ul>
<li>In Windows, check the hard drive status in Disk Management (Windows and X keys on the keyboard, select Disk Management).</li>
<li>In macOS, check the hard drive status in Disk Utility (Finder &gt; Applications &gt; Utilities &gt; Disk Utility).</li>
</ul>
</li>
<li>The drive may be corrupted. Reformatting your drive can solve this issue.<br />
<ul>
<li><a href="https://www.seagate.com/support/kb/how-to-format-your-hard-drive-220151en/">Instructions for formatting</a>.<br /><em>Please be aware that formatting will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.</em></li>
</ul>
</li>
<li>Is the LED light of the hard drive turning on? What is its behavior: It is always off, always on or does it blink?</li>
<li>Put the hand over the drive, does it feel a slight movement like spinning? Does it feel hard or doesn&rsquo;t feel at all?</li>
<li>If the drive is making strange noises, like clicking or buzzing, or have slower performance than the normal, please disconnect immediately the drive from the computer and reply to this email.</li>
<li>Follow this <a href="https://www.seagate.com/support/kb/seagate-usb-external-drive-diagnostics-006153en/">guide in Seagate for more advanced troubleshooting.</a></li>
</ul>
```
---

Speed note:

Including basic port-computer-cable troubleshooting for external drive, including review with Disk Utility under macOS or Disk Management under Windows, including a link for formatting instructions with data loss script. Requesting disk usage and current data status, suggesting to reply email to check recovery options.
