### [EN-GEN-TSs] Windows drive formatting-Lite

Overview:

Lighter template for Windows drive formatting template.

---

Preview:

<p>In this case is recommended to format the hard drive for Windows, follow the instructions for formatting in this <a href="https://www.seagate.com/support/kb/how-to-format-your-drive-on-windows/">support page from Seagate</a>.</p>

---

Plain text:

```
In this case is recommended to format the hard drive for Windows, follow the instructions for formatting in this support page from Seagate:
https://www.seagate.com/support/kb/how-to-format-your-drive-on-windows/.
```

---

HTML code:

```
<p>In this case is recommended to format the hard drive for Windows, follow the instructions for formatting in this <a href="https://https://www.seagate.com/support/kb/how-to-format-your-drive-on-windows/">support page from Seagate</a>.</p>
```

---

Speed note:

Included link to Seagate support for instructions for formatting in Windows.
