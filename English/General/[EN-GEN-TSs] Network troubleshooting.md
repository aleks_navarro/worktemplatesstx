### [EN-GEN-TSs] Network troubleshooting

Overview:

Instructions to verify network settings and configuration, includes the port forwarding, use of flush dns, ipconfig settings and tracert to check the route of the NAS and static IP address. This is mainly used in Biz-NAS.

---

Preview:

<p>Please the following network configurations and status:</p>
<ul>
<li>Check the port forwarding in the firewall of the router and computer (check user manual of each device for this), verify that the NAS is not blocked and be sure that the ports 80, 443, 445, 8080, 21 and 22 are open.</li>
<li>If there are connection problems, flush the IP and DNS settings in Windows 10: in CMD enter the commands "ipconfig /flushdns" and "netsh int ip reset", restart the computer.</li>
<li>Try to trace route the NAS, execute the following command in CMD: "tracert nas_ip_address".</li>
<li>Preferably the NAS should have a designated static IP address, verify in your router if this setting is configured or enabled for the NAS.</li>
</ul>

---

Plain text:

```
Please the following network configurations and status:
Check the port forwarding in the firewall of the router and computer (check user manual of each device for this), verify that the NAS is not blocked and be sure that the ports 80, 443, 445, 8080, 21 and 22 are open.
If there are connection problems, flush the IP and DNS settings in Windows 10: in CMD enter the commands "ipconfig /flushdns" and "netsh int ip reset", restart the computer.
Try to trace route the NAS, execute the following command in CMD: "tracert nas_ip_address".
Preferably the NAS should have a designated static IP address, verify in your router if this setting is configured or enabled for the NAS.
```
---

HTML code:

```
<p>Please the following network configurations and status:</p>
<ul>
<li>Check the port forwarding in the firewall of the router and computer (check user manual of each device for this), verify that the NAS is not blocked and be sure that the ports 80, 443, 445, 8080, 21 and 22 are open.</li>
<li>If there are connection problems, flush the IP and DNS settings in Windows 10: in CMD enter the commands "ipconfig /flushdns" and "netsh int ip reset", restart the computer.</li>
<li>Try to trace route the NAS, execute the following command in CMD: "tracert nas_ip_address".</li>
<li>Preferably the NAS should have a designated static IP address, verify in your router if this setting is configured or enabled for the NAS.</li>
</ul>
```
---

Speed note:

Included instructions to verify network settings, port forwarding, use of ipconfig, tracert and static IP address for NAS.
