### [EN-GEN-CSs] Lifespan explanation

Overview:

This template provides a simple explanation of the hard drive lifespan.

---

Preview:

<p>With regard to the warranty and lifespan of the drive: warranty of your drive was of two years but it doesn&rsquo;t have strictly to reflect the lifespan of the device. Regularly lifespan of the drives are more determined by the conditions of use and by matters of chance; in the correct using conditions the drive can last even more than two years, sometimes it is only a matter of chance and this cases of short lifespans are not the norm, usually a drive can last for five or even more years.</p>

---

Plain text:

```
With regard to the warranty and lifespan of the drive: warranty of your drive was of two years but it doesn’t have strictly to reflect the lifespan of the device. Regularly lifespan of the drives are more determined by the conditions of use and by matters of chance; in the correct using conditions the drive can last even more than two years, sometimes it is only a matter of chance and this cases of short lifespans are not the norm, usually a drive can last for five or even more years.
```
---

HTML code:

```
<p>With regard to the warranty and lifespan of the drive: warranty of your drive was of two years but it doesn&rsquo;t have strictly to reflect the lifespan of the device. Regularly lifespan of the drives are more determined by the conditions of use and by matters of chance; in the correct using conditions the drive can last even more than two years, sometimes it is only a matter of chance and this cases of short lifespans are not the norm, usually a drive can last for five or even more years.</p>
```
---

Speed note:

Explained the lifespan of the hard drives.
