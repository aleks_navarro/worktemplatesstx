### [EN-GEN-TSs] Binary_vs_Decimal

Overview:

This template is used when is asked why the capacity of the hard drive is different in the system than the published in package.

---

Preview:

<p>I understand that the drive's capacity appears to be less than advertised. This is due to a translation of a decimal number system to the binary number system that the operating system utilizes.</p>
<p>Here are two Knowledge Base articles, which explain this thoroughly:</p>
<ul>
<li><a href="https://www.seagate.com/la/es/support/kb/storage-capacity-measurement-standards-194563en/">Storage capacity measurement standards</a></li>
<li><a href="https://www.seagate.com/support/kb/why-does-my-hard-drive-report-less-capacity-than-indicated-on-the-drives-label-172191en/">Why does my hard drive report less capacity than indicated on the drive's label?</a></li>
</ul>

---

Plain text:

```
I understand that the drive's capacity appears to be less than advertised. This is due to a translation of a decimal number system to the binary number system that the operating system utilizes.

Here are two Knowledge Base articles, which explain this thoroughly:
http://knowledge.seagate.com/articles/en_US/FAQ/194563en
http://knowledge.seagate.com/articles/en_US/FAQ/172191en
```
---

HTML code:

```
<p>I understand that the drive's capacity appears to be less than advertised. This is due to a translation of a decimal number system to the binary number system that the operating system utilizes.</p>
<p>Here are two Knowledge Base articles, which explain this thoroughly:</p>
<ul>
<li><a href="https://www.seagate.com/la/es/support/kb/storage-capacity-measurement-standards-194563en/">Storage capacity measurement standards</a></li>
<li><a href="https://www.seagate.com/support/kb/why-does-my-hard-drive-report-less-capacity-than-indicated-on-the-drives-label-172191en/">Why does my hard drive report less capacity than indicated on the drive's label?</a></li>
</ul>
```
---

Speed note:

Explained to customer the difference between binary and decimal and why the capacity of the hard drive is different than package indicates, including links to Seagate knowledge.
