### [EN-GEN-TSs] File History in MSWin - Lite

Overview:

Lighter alternative to the template File History in MSWin.

---

Preview:
<p>Also, you can try File History for backup management, which is a tool native to Windows. You can find more info about File History in Microsoft Support:</p>
<ul>
<li><a href="https://support.microsoft.com/en-us/help/17128/windows-8-file-history">for Windows 8/8.1</a>,</li>
<li><a href="https://support.microsoft.com/en-us/help/4027408/windows-10-backup-and-restore">for Windows 10</a>.</li>
</ul>
<p><em>Please be aware that the provided URL is not a Seagate maintained or monitored website.</em></p>

---

Plain text:

```
Also, you can try File History for backup management, which is a tool native to Windows. You can find more info about File History in Microsoft Support:
for Windows 8/8.1
https://support.microsoft.com/en-us/help/17128/windows-8-file-history,
for Windows 10
https://support.microsoft.com/en-us/help/4027408/windows-10-backup-and-restore.
Please be aware that the provided URL is not a Seagate maintained or monitored website.
```
---

HTML code:

```
<p>Also, you can try File History for backup management, which is a tool native to Windows. You can find more info about File History in Microsoft Support:</p>
<ul>
<li><a href="https://support.microsoft.com/en-us/help/17128/windows-8-file-history">for Windows 8/8.1</a>,</li>
<li><a href="https://support.microsoft.com/en-us/help/4027408/windows-10-backup-and-restore">for Windows 10</a>.</li>
</ul>
<p><em>Please be aware that the provided URL is not a Seagate maintained or monitored website.</em></p>
```
---

Speed note:

Suggested File History as an alternative, included links to MS Support for more information with third party url script.
