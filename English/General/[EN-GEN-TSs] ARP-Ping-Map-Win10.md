### [EN-GEN-TSs] ARP-Ping-Map-Win10

Overview:

This template is a set of instructions divided intro three sections with three instructions each one. Each set contains instructions to enable SMB1, use of the commands Arp and Ping to find a NAS device, and to Map using File Explorer. The instructions are focused to be used on Windows 10, however most instructions to do this setup are the same since Windows Vista. This template can be broken into its three parts to be used individually when needed.

---

Preview:

<p>We are going to try to find the device in the network, mapping it and access it. Follow the next guide:</p>
<p>SMB1 is disabled by default in Windows 10. We can try to enable this communication protocol, but keep in mind that this is disabled due security issues, so keep security measures like antivirus and firewall updated and configured.</p>
<ol>
<li>On your Windows 10 computer, please enable the SMB protocol (if not enabled already).Go to "Control Panel &gt; Programs &gt; Turn Windows features on or off".</li>
<li>Expand the "SMB 1.0/CIFS File Sharing Support" option. Check the "SMB 1.0/CIFS Client" option. (Make sure all the options under the SMB are checked, including Server)</li>
<li>Click the "OK" button. Restart your computer.<br />Note: administrator permissions may be required to perform this operation.</li>
</ol>
<p>We are going to find the device using "arp" command and verify connection with ping. Follow the next steps.</p>
<ol>
<li>Click on the Search symbol beside the Start menu on your Windows. In the Search bar, type in "cmd" or "Command Prompt", and press Enter.</li>
<li>In the command prompt window, type "arp -a" and press Enter. It should now show a list of internet addresses and physical addresses. Check to see if the MAC Address of the device is visible in the Physical Addresses list along its IP address.</li>
<li>In the command prompt window, type &ldquo;ping IP_address&rdquo;, where IP_address is the device IP address listed in the previous step.<br />Note: the MAC address of the device is printed in the label of the device located at the bottom of it.</li>
</ol>
<p>We are going to map the device so it is available to explore in the File Explorer of Windows.</p>
<ol>
<li>Open File Explorer from the taskbar or the Start menu, or press the Windows logo key + E. Select This PC from the left pane.</li>
<li>On the Computer tab, select Map network drive. In the Drive list, select a drive letter. (Any available letter will do.)</li>
<li>In the Folder box, type the path of the folder or computer, or select Browse to find the folder or computer. To connect every time you log on to your PC, select the Reconnect at sign-in check box. Select Finish.<br />Note: If you can't connect to a network drive or folder, the computer you're trying to connect to might be turned off, or you might not have the correct permissions. Try contacting your network administrator.</li>
</ol>

---

Plain text:

```
We are going to try to find the device in the network, mapping it and access it. Follow the next guide:

SMB1 is disabled by default in Windows 10. We can try to enable this communication protocol, but keep in mind that this is disabled due security issues, so keep security measures like antivirus and firewall updated and configured.
On your Windows 10 computer, please enable the SMB protocol (if not enabled already).
1. Go to "Control Panel > Programs > Turn Windows features on or off".
2. Expand the "SMB 1.0/CIFS File Sharing Support" option. Check the "SMB 1.0/CIFS Client" option. (Make sure all the options under the SMB are checked, including Server)
3. Click the "OK" button. Restart your computer.
Note: administrator permissions may be required to perform this operation.

We are going to find the device using "arp" command and verify connection with ping. Follow the next steps.
1. Click on the Search symbol beside the Start menu on your Windows. In the Search bar, type in "cmd" or "Command Prompt", and press Enter.
2. In the command prompt window, type "arp -a" and press Enter. It should now show a list of internet addresses and physical addresses. Check to see if the MAC Address of the device is visible in the Physical Addresses list along its IP address.
3. In the command prompt window, type “ping IP_address”, where IP_address is the device IP address listed in the previous step.
Note: the MAC address of the device is printed in the label of the device located at the bottom of it.

We are going to map the device so it is available to explore in the File Explorer of Windows.
1. Open File Explorer from the taskbar or the Start menu, or press the Windows logo key + E. Select This PC from the left pane.
2. On the Computer tab, select Map network drive. In the Drive list, select a drive letter. (Any available letter will do.)
3. In the Folder box, type the path of the folder or computer, or select Browse to find the folder or computer. To connect every time you log on to your PC, select the Reconnect at sign-in check box. Select Finish.
Note: If you can't connect to a network drive or folder, the computer you're trying to connect to might be turned off, or you might not have the correct permissions. Try contacting your network administrator.
```
---

HTML code:

```
<p>We are going to try to find the device in the network, mapping it and access it. Follow the next guide:</p>
<p>SMB1 is disabled by default in Windows 10. We can try to enable this communication protocol, but keep in mind that this is disabled due security issues, so keep security measures like antivirus and firewall updated and configured.</p>
<ol>
<li>On your Windows 10 computer, please enable the SMB protocol (if not enabled already).Go to "Control Panel &gt; Programs &gt; Turn Windows features on or off".</li>
<li>Expand the "SMB 1.0/CIFS File Sharing Support" option. Check the "SMB 1.0/CIFS Client" option. (Make sure all the options under the SMB are checked, including Server)</li>
<li>Click the "OK" button. Restart your computer.<br />Note: administrator permissions may be required to perform this operation.</li>
</ol>
<p>We are going to find the device using "arp" command and verify connection with ping. Follow the next steps.</p>
<ol>
<li>Click on the Search symbol beside the Start menu on your Windows. In the Search bar, type in "cmd" or "Command Prompt", and press Enter.</li>
<li>In the command prompt window, type "arp -a" and press Enter. It should now show a list of internet addresses and physical addresses. Check to see if the MAC Address of the device is visible in the Physical Addresses list along its IP address.</li>
<li>In the command prompt window, type &ldquo;ping IP_address&rdquo;, where IP_address is the device IP address listed in the previous step.<br />Note: the MAC address of the device is printed in the label of the device located at the bottom of it.</li>
</ol>
<p>We are going to map the device so it is available to explore in the File Explorer of Windows.</p>
<ol>
<li>Open File Explorer from the taskbar or the Start menu, or press the Windows logo key + E. Select This PC from the left pane.</li>
<li>On the Computer tab, select Map network drive. In the Drive list, select a drive letter. (Any available letter will do.)</li>
<li>In the Folder box, type the path of the folder or computer, or select Browse to find the folder or computer. To connect every time you log on to your PC, select the Reconnect at sign-in check box. Select Finish.<br />Note: If you can't connect to a network drive or folder, the computer you're trying to connect to might be turned off, or you might not have the correct permissions. Try contacting your network administrator.</li>
</ol>
```
---

Speed note:

Included the instructions to setup a NAS in Windows 10: enable SMB1, use of Arp and Ping, and Map with File Explorer.
