### [EN-GEN-TSs] ARP-Ping-Map-MacOS

Overview:

Instructions for NAS in macOS, from ARP, ping and map a share or drive in macOS. Five step tutorial.

---

Preview:

<p>We can try to find the device in the network and map it again on the computer. Please check that the device is connected to the power outlet and local network router and follow the next guide:</p>
<ol>
<li>Make sure that you see Finder in the top left corner of your screen then click on Go then "Utilities" &gt; "Terminal".</li>
<li>The Terminal window will open. Type in "arp -a" then hit Enter.</li>
<li>It should now show a list of internet addresses and physical addresses. Check to see if the MAC Address (which should be physically on the device) of the device is visible in the Physical Addresses list.</li>
<li>Now that you have the IP address of the drive make sure that you see Finder in the top left corner of your screen then click on Go then Connect to Server.<br />In the Connect to Server window, type: "cifs://IP Address" (ex. "cifs://192.168.1.85").<br />Note: Make sure you use a forward slash (/) and not a backslash (\) . Then select Connect.</li>
<li>When prompted, enter your username and password and select Connect. The drive is now mapped and can be accessed under "Finder" &gt; "Shared".<br />Note: By default, your Mac will auto-populate the username field with the Mac's username. This information will need to be changed back to the Seagate Consumer NAS username.</li>
</ol>

---

Plain text:

```
We can try to find the device in the network and map it again on the computer. Please check that the device is connected to the power outlet and local network router and follow the next guide:
1. Make sure that you see Finder in the top left corner of your screen then click on Go then "Utilities" > "Terminal".
2. The Terminal window will open. Type in "arp -a" then hit Enter.
3. It should now show a list of internet addresses and physical addresses. Check to see if the MAC Address (which should be physically on the device) of the device is visible in the Physical Addresses list.
4. Now that you have the IP address of the drive make sure that you see Finder in the top left corner of your screen then click on Go then Connect to Server.
5. In the Connect to Server window, type: "cifs://IP Address" (ex. "cifs://192.168.1.85"). Note: Make sure you use a forward slash (/) and not a backslash (\) . Then select Connect.
6. When prompted, enter your username and password and select Connect. The drive is now mapped and can be accessed under "Finder" > "Shared".
Note: By default, your Mac will auto-populate the username field with the Mac's username. This information will need to be changed back to the Seagate Consumer NAS username.
```
---

HTML code:

```
<p>We can try to find the device in the network and map it again on the computer. Please check that the device is connected to the power outlet and local network router and follow the next guide:</p>
<ol>
<li>Make sure that you see Finder in the top left corner of your screen then click on Go then "Utilities" &gt; "Terminal".</li>
<li>The Terminal window will open. Type in "arp -a" then hit Enter.</li>
<li>It should now show a list of internet addresses and physical addresses. Check to see if the MAC Address (which should be physically on the device) of the device is visible in the Physical Addresses list.</li>
<li>Now that you have the IP address of the drive make sure that you see Finder in the top left corner of your screen then click on Go then Connect to Server.<br />In the Connect to Server window, type: "cifs://IP Address" (ex. "cifs://192.168.1.85").<br />Note: Make sure you use a forward slash (/) and not a backslash (\) . Then select Connect.</li>
<li>When prompted, enter your username and password and select Connect. The drive is now mapped and can be accessed under "Finder" &gt; "Shared".<br />Note: By default, your Mac will auto-populate the username field with the Mac's username. This information will need to be changed back to the Seagate Consumer NAS username.</li>
</ol>
```
---

Speed note:
Included instructions for connecting a NAS to macOS by using the Terminal and Finder, from ARP, ping and Map.
