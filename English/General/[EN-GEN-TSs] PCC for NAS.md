### [EN-GEN-TSs] PCC for NAS

Overview:

This template is a basic troubleshooting for NAS devices, it includes some RAID questions. Works for both Con and Biz -NAS. See also: Network troubleshooting, Arp-Ping-Map.

---

Preview:

<p>We are going to try the following troubleshooting guide to determine if there is no problem with the device itself. Please check and answer the following:</p>
<ul>
<li>What is the behaviour of the LEDs of your device? Please inform us about any light change.</li>
<li>Can you please try to plug your server to another network or connect the device directly to the computer?</li>
<li>Have you tried a different computer, data cables, ports, and different power adapters or outlets to connect your device to?</li>
<li>Can you please ensure that the NAS is properly connected to your local network?</li>
<li>Can you please check that the Ethernet cable is connected and functioning properly?</li>
<li>Can you please verify that the NAS is compatible with your router? For this, you can contact the original manufacturer of your router.</li>
<li>Have you made any modifications to your Operating System or router recently?</li>
<li>Can you see any files/folders on the device and if so, can you open the files/folders?</li>
<li>Are you getting any on-screen messages or errors?</li>
<li>Do you hear any unusual noise coming out of the device?</li>
<li>Is there any RAID configuration in the NAS? (RAID 0, RAID 1&hellip;)</li>
<li>Are you using LaCie RAID Manager to configure your hard drive array?</li>
<li>Is any of your disk drives seen under LaCie RAID Manager?</li>
</ul>

---

Plain text:

```
We are going to try the following troubleshooting guide to determine if there is no problem with the device itself. Please check and answer the following:
What is the behaviour of the LEDs of your device? Please inform us about any light change.
Can you please try to plug your server to another network or connect the device directly to the computer?
Have you tried a different computer, data cables, ports, and different power adapters or outlets to connect your device to?
Can you please ensure that the NAS is properly connected to your local network?
Can you please check that the Ethernet cable is connected and functioning properly?
Can you please verify that the NAS is compatible with your router? For this, you can contact the original manufacturer of your router.
Have you made any modifications to your Operating System or router recently?
Can you see any files/folders on the device and if so, can you open the files/folders?
Are you getting any on-screen messages or errors?
Do you hear any unusual noise coming out of the device?
Is there any RAID configuration in the NAS? (RAID 0, RAID 1…)
Are you using LaCie RAID Manager to configure your hard drive array?
Is any of your disk drives seen under LaCie RAID Manager?
```
---

HTML code:

```
<p>We are going to try the following troubleshooting guide to determine if there is no problem with the device itself. Please check and answer the following:</p>
<ul>
<li>What is the behaviour of the LEDs of your device? Please inform us about any light change.</li>
<li>Can you please try to plug your server to another network or connect the device directly to the computer?</li>
<li>Have you tried a different computer, data cables, ports, and different power adapters or outlets to connect your device to?</li>
<li>Can you please ensure that the NAS is properly connected to your local network?</li>
<li>Can you please check that the Ethernet cable is connected and functioning properly?</li>
<li>Can you please verify that the NAS is compatible with your router? For this, you can contact the original manufacturer of your router.</li>
<li>Have you made any modifications to your Operating System or router recently?</li>
<li>Can you see any files/folders on the device and if so, can you open the files/folders?</li>
<li>Are you getting any on-screen messages or errors?</li>
<li>Do you hear any unusual noise coming out of the device?</li>
<li>Is there any RAID configuration in the NAS? (RAID 0, RAID 1&hellip;)</li>
<li>Are you using LaCie RAID Manager to configure your hard drive array?</li>
<li>Is any of your disk drives seen under LaCie RAID Manager?</li>
</ul>
```
---

Speed note:

Including basic port-cable-computer for NAS, requesting current status of the device in the network as the current state of physical connections and software management as well.
