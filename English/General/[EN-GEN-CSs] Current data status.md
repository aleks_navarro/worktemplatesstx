### [EN-GEN-CSs] Current data status

Overview:

Use this questions when is required to know more about the current status of the customer device and the data stored on it.

---

Preview:
<p>As additional questions about the disk usage for more accurate technical support:</p>
<ul>
<li>How was the drive being used? Was detected a previous strange behaviour?</li>
<li>What is the current state of the data contained in the drive? Does it have a backup or there is a risk of data loss?</li>
<li>Was it being used with encryption software for the data?</li>
</ul>
For checking and reviewing advanced data recovery options please reply to this email.

---

Plain text:

```
As additional questions about the disk usage for more accurate technical support:
How was the drive being used? Was detected a previous strange behaviour?
What is the current state of the data contained in the drive? Does it have a backup or there is a risk of data loss?
Was it being used with encryption software for the data?
For checking and reviewing advanced data recovery options please reply to this email.
```
---

HTML code:

```
<p>As additional questions about the disk usage for more accurate technical support:</p>
<ul>
<li>How was the drive being used? Was detected a previous strange behaviour?</li>
<li>What is the current state of the data contained in the drive? Does it have a backup or there is a risk of data loss?</li>
<li>Was it being used with encryption software for the data?</li>
</ul>
For checking and reviewing advanced data recovery options please reply to this email.
```
---

Speed note:

Requested current device usage and data status.
