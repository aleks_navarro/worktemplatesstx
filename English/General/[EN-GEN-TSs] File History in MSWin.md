### [EN-GEN-TSs] File History in MSWin

Overview:

File history is a backup software native to Microsoft Windows to manage backups, is an alternative (or main option) to Toolkit.

---

Preview:

<p>Also, you can try File History, which is a tool native to Windows for managing backups.</p>
<ol>
<li>Open Settings, and click/tap on the Update &amp; security icon.</li>
<li>Click on Backup on the left side, and click/tap on the More options on the right side.</li>
<li>Click on the Back up now button.</li>
<li>File History will now create a new backup version of your files from your added folders.</li>
<li>When finished, you can close Settings if you like.</li>
</ol>
<p>More info regarding to File History in Microsoft Support:</p>
<ul>
<li><a href="https://support.microsoft.com/en-us/help/17128/windows-8-file-history">for Windows 8/8.1</a>,</li>
<li><a href="https://support.microsoft.com/en-us/help/4027408/windows-10-backup-and-restore">for Windows 10</a>.</li>
</ul>
<p><em>Please be aware that the provided URL is not a Seagate maintained or monitored website.</em></p>

---

Plain text:

```
Also, you can try File History, which is a tool native to Windows for managing backups.
1. Open Settings, and click/tap on the Update & security icon.
2. Click on Backup on the left side, and click/tap on the More options on the right side.
3. Click on the Back up now button.
3. File History will now create a new backup version of your files from your added folders.
4. When finished, you can close Settings if you like.

More info regarding to File History in Microsoft Support:
for Windows 8/8.1
https://support.microsoft.com/en-us/help/17128/windows-8-file-history,
for Windows 10
https://support.microsoft.com/en-us/help/4027408/windows-10-backup-and-restore.
Please be aware that the provided URL is not a Seagate maintained or monitored website.
```
---

HTML code:

```
<p>Also, you can try File History, which is a tool native to Windows for managing backups.</p>
<ol>
<li>Open Settings, and click/tap on the Update &amp; security icon.</li>
<li>Click on Backup on the left side, and click/tap on the More options on the right side.</li>
<li>Click on the Back up now button.</li>
<li>File History will now create a new backup version of your files from your added folders.</li>
<li>When finished, you can close Settings if you like.</li>
</ol>
<p>More info regarding to File History in Microsoft Support:</p>
<ul>
<li><a href="https://support.microsoft.com/en-us/help/17128/windows-8-file-history">for Windows 8/8.1</a>,</li>
<li><a href="https://support.microsoft.com/en-us/help/4027408/windows-10-backup-and-restore">for Windows 10</a>.</li>
</ul>
<p><em>Please be aware that the provided URL is not a Seagate maintained or monitored website.</em></p>
```
---

Speed note:

Provided instructions to the customer to configure File History in Windows, including links to MS Support with third party url script.
