### [EN-GEN-TSs] Toolkit resources

Overview:

Links to Seagate for Toolkit resources.

---

Preview:
<p>As a final suggestion, it is recommended to manage backups using Toolkit, which is the currently supported backup tool by Seagate/LaCie:</p>
<ul>
<li><a href="https://www.seagate.com/support/downloads/">Download Toolkit</a></li>
<li><a href="https://www.seagate.com/manuals/software/toolkit/">Toolkit User Manual</a></li>
</ul>

---

Plain text:

```
As a final suggestion, it is recommended to manage backups using Toolkit, which is the currently supported backup tool by Seagate/LaCie.
Download Toolkit
https://www.seagate.com/support/downloads/.
Toolkit User Manual
https://www.seagate.com/manuals/software/toolkit/.
```
---

HTML code:

```
<p>As a final suggestion, it is recommended to manage backups using Toolkit, which is the currently supported backup tool by Seagate/LaCie:</p>
<ul>
<li><a href="https://www.seagate.com/support/downloads/">Download Toolkit</a></li>
<li><a href="https://www.seagate.com/manuals/software/toolkit/">Toolkit User Manual</a></li>
</ul>
```
---

Speed note:

Included links to Seagate for Toolkit resources, support page, download and user manual.
