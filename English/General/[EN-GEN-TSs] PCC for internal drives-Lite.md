### [EN-GEN-TSs] PCC for internal drives-Lite

Overview:

This is a template which resumes in one link from Seagate Support a PCC for internal drives. Suggested to be used along another troubleshooter, like SeaTools Booteable, to avoid a wall of text.

---

Preview:

<p>To review and check another possible issues with your hard drive please try following the <a href="https://www.seagate.com/support/kb/internal-hard-drive-troubleshooter-006183en/">troubleshooting guide for internal drives</a>.<br />Please reply to this email with the results of the troubleshooting.</p>

---

Plain text:

```
To review and check another possible issues with your hard drive please try following the troubleshooting guide for internal drives:
https://www.seagate.com/support/kb/usb-external-troubleshooter-003581en
Please reply to this email with the results of the troubleshooting.
```
---

HTML code:

```
<p>To review and check another possible issues with your hard drive please try following the <a href="https://www.seagate.com/support/kb/internal-hard-drive-troubleshooter-006183en/">troubleshooting guide for internal drives</a>.<br />Please reply to this email with the results of the troubleshooting.</p>
```
---

Speed note:

Included a link to Seagate Support for Internal hard drive Troubleshooter.
