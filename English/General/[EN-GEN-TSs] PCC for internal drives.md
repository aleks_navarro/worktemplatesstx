### [EN-GEN-TSs] PCC for internal drives

Overview:

A port-cable-computer guide for internal drives, it includes steps as formatting and check in BIOS. It does not include current data status review and should be used separately. This template is focused mostly on Windows, however when necessary can be included some instructions for Unix and GNU/Linux with a no-official support statement. See also: PCC external USB Diagnostics, SATA to USB adapter.

---

Preview:
<p>We are going to try the following troubleshooting guide to determine if there is no problem with the drive itself. Please check and answer the following:</p>
<ul>
<li>Try to use another SATA cable, similar or equal, for connecting your drive.</li>
<li>Try to connect it to another port on your computer or another computer.</li>
<li>Check the compatibility of the drive with your motherboard and/or hardware configuration.</li>
<li>Check the hard drive status on the computer BIOS. To access the BIOS check the hardware manufacturer documentation and user manuals.</li>
<li>Check the hard drive status in Disk Management (Windows and X keys on the keyboard, select Disk Management).</li>
<li>The drive may be corrupted. Reformatting your drive can solve this issue.<br />
<ul>
<li><a href="https://www.seagate.com/support/kb/how-to-format-your-hard-drive-220151en/">Instructions for formatting</a>.<br /><em>Please be aware that formatting will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.</em></li>
</ul>
</li>
<li>Heard carefully at the drive, is there any sound like slight movement like spinning? Does it sound hard or doesn&rsquo;t sound at all?</li>
<li>If the drive is making strange noises, like clicking or buzzing, or have slower performance than the normal, please disconnect immediately the drive from the computer and reply to this email.</li>
</ul>

---

Plain text:

```
We are going to try the following troubleshooting guide to determine if there is no problem with the drive itself. Please check and answer the following:
Try to use another SATA cable, similar or equal, for connecting your drive.
Try to connect it to another port on your computer or another computer.
Check the compatibility of the drive with your motherboard and/or hardware configuration.
Check the hard drive status on the computer BIOS. To access the BIOS check the hardware manufacturer documentation and user manuals.
Check the hard drive status in Disk Management (Windows and X keys on the keyboard, select Disk Management).
The drive may be corrupted. Reformatting your drive can solve this issue.
Instructions for formatting
https://www.seagate.com/support/kb/how-to-format-your-hard-drive-220151en/.
Please be aware that formatting will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.
Heard carefully at the drive, is there any sound like slight movement like spinning? Does it sound hard or doesn’t sound at all?
If the drive is making strange noises, like clicking or buzzing, or have slower performance than the normal, please disconnect immediately the drive from the computer and reply to this email.
```
---

HTML code:

```
<p>We are going to try the following troubleshooting guide to determine if there is no problem with the drive itself. Please check and answer the following:</p>
<ul>
<li>Try to use another SATA cable, similar or equal, for connecting your drive.</li>
<li>Try to connect it to another port on your computer or another computer.</li>
<li>Check the compatibility of the drive with your motherboard and/or hardware configuration.</li>
<li>Check the hard drive status on the computer BIOS. To access the BIOS check the hardware manufacturer documentation and user manuals.</li>
<li>Check the hard drive status in Disk Management (Windows and X keys on the keyboard, select Disk Management).</li>
<li>The drive may be corrupted. Reformatting your drive can solve this issue.<br />
<ul>
<li><a href="https://www.seagate.com/support/kb/how-to-format-your-hard-drive-220151en/">Instructions for formatting</a>.<br /><em>Please be aware that formatting will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.</em></li>
</ul>
</li>
<li>Heard carefully at the drive, is there any sound like slight movement like spinning? Does it sound hard or doesn&rsquo;t sound at all?</li>
<li>If the drive is making strange noises, like clicking or buzzing, or have slower performance than the normal, please disconnect immediately the drive from the computer and reply to this email.</li>
</ul>
```
---

Speed note:
Including basic port-computer-cable troubleshooting for internal drive, including review with Disk Management under Windows and/or/nor with commands in Linux (specifying out of support for this case). Including a link for formatting instructions with data loss script.
