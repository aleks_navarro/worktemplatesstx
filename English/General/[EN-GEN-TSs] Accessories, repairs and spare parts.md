### [EN-GEN-TSs] Accessories, repairs and spare parts

Overview:

Brief explanation when an accessory, repair or hardware part is requested.

---

Preview:

<p>We at Seagate/LaCie, don&rsquo;t make reparations of drives or provide accessories, such as power supplies or USB cables, or hardware spare parts.</p>

---

Plain text:

```
We at Seagate/LaCie, don’t make reparations of drives or provide accessories, such as power supplies or USB cables, or hardware spare parts.
```
---

HTML code:

```
<p>We at Seagate/LaCie, don&rsquo;t make reparations of drives or provide accessories, such as power supplies or USB cables, or hardware spare parts.</p>
```
---

Speed note:

Explained that we do not provide accessories, repairs and spare hardware as support.
