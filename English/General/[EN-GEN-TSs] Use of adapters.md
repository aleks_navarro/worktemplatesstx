### [EN-GEN-TSs] Use of adapters

Overview:

Explanation regarding the using of adapters for connecting drives, like in USB-C to USB-A. This does not apply to SATA to USB adapters.

---

Preview:

<p>It is not recommended trying to connect the device using adapters since this adaptations may not deliver enough power to the device to work normally. You can try to connect the device using this adapter but it is not guaranteed its full support.</p>

---

Plain text:

```
It is not recommended trying to connect the device using adapters since this adaptations may not deliver enough power to the device to work normally. You can try to connect the device using this adapter but it is not guaranteed its full support.
```
---

HTML code:

```
<p>It is not recommended trying to connect the device using adapters since this adaptations may not deliver enough power to the device to work normally. You can try to connect the device using this adapter but it is not guaranteed its full support.</p>
```
---

Speed note:

Explained the use of adapters for connecting the drive.
