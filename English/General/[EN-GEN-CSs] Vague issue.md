### [EN-GEN-CSs] Vague issue

Overview:

A template requesting more details to the customer about a vague case or with no so much details.

---

Preview:

<p>I understand that you are having some trouble with your product. What trouble are you having with it, specifically? What are you doing when the issue occurs? Do you get an error message (what is the error)? Is the drive not detected?<br />If you could provide more details on the trouble you are having, this will help us diagnose your issue.</p>

---

Plain text:

```
I understand that you are having some trouble with your product. What trouble are you having with it, specifically? What are you doing when the issue occurs? Do you get an error message (what is the error)? Is the drive not detected?
If you could provide more details on the trouble you are having, this will help us diagnose your issue.
```
---

HTML code:

```
<p>I understand that you are having some trouble with your product. What trouble are you having with it, specifically? What are you doing when the issue occurs? Do you get an error message (what is the error)? Is the drive not detected?<br />If you could provide more details on the trouble you are having, this will help us diagnose your issue.</p>
```
---

Speed note:

Requesting more case details to the customer.
