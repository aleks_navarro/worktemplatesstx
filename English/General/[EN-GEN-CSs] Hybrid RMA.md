### [EN-GEN-CSs] Hybrid RMA

Overview:

Use this template to to create an online RMA or via email by requesting to customer the information.

---

Preview:

<div>Analyzing your information it seems that your drive has symptoms of failure. Unusual noises (such as clicks, grinding, whining, etc) are the most common indicators of physical drive failure, especially if the drive has started malfunctioning or causing logical errors. Please keep your drive disconnected from the computer to prevent further damage to the disk or the information stored in it.</div>
<div>&nbsp;</div>
<div>Important: Before proceeding with the warranty replacement order, be aware that data recovery is not covered under the warranty and by proceeding directly to the warranty replacement the data in the hard drive will be lost. For checking and reviewing advanced data recovery options please reply to this email.</div>
<div><br />After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}. We will be happy to replace your drive and we're sorry for all the trouble this may have caused.</div>
<div>To check terms and conditions and start a warranty replacement process online please go to <a href="http://www.seagate.com/support/warranty-and-replacements/">Seagate Warranty and Replacements</a>.</div>
<div>&nbsp;</div>
<div>Otherwise, we can place the order of warranty replacement for you, for this please verify and fill-in the following information:</div>
<ul>
<li>Full name (first name and last name)</li>
<li>Phone number for contact</li>
<li>Valid email address</li>
<li>Name of the company (if applicable)</li>
<li>Address, city, postal code, state/Province</li>
<li>Is the address residential or commercial?</li>
<li>Full billing address (only if it&rsquo;s different from the shipping address)</li>
<li>Would you like to receive updates via SMS?</li>
<li>Do you agree with terms and conditions (see below)?</li>
</ul>
<div>&nbsp;</div>
<div>Once the replacement request is finished, you will receive an email with instructions for the replacement. Please read them carefully and follow them.<br />And when drive has been sent to our warehouse, keep the tracking number of the package and if there is any problem or question regarding the replacement process then please reply to this email attaching the tracking number and the RMA number.</div>
<div>&nbsp;</div>
<div>For the replacement order you agree to:</div>
<div>Your continuance of the product return process means that you do agree to the Seagate privacy policy and Seagate online terms and conditions available at the legal page on our website at&nbsp;www.seagate.com, and to the return policy, shipping instructions, and E-Commerce &amp; RMA Terms available on the support pages of our website. You will be provided an order acknowledgment following submission of your order, giving you the ability to review the entirety of these online terms and contact Seagate if you have any questions, or cancel your order if you do not agree to these terms.<br />Your continuance of the product return process means that you do agree that Seagate products may not be exported to restricted countries, or used to develop, produce or distribute chemical, biological, or nuclear weapons, missile technologies, or other terrorist activities.<br />Please be advised, Seagate may replace your product with a product that was previously used, repaired, and tested to meet Seagate specifications.</div>

---

Plain text:

```
Analyzing your information it seems that your drive has symptoms of failure. Unusual noises (such as clicks, grinding, whining, etc) are the most common indicators of physical drive failure, especially if the drive has started malfunctioning or causing logical errors. Please keep your drive disconnected from the computer to prevent further damage to the disk or the information stored in it.

Important: Before proceeding with the warranty replacement order, be aware that data recovery is not covered under the warranty and by proceeding directly to the warranty replacement the data in the hard drive will be lost. For checking and reviewing advanced data recovery options please reply to this email.

After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}. We will be happy to replace your drive and we're sorry for all the trouble this may have caused.
To check terms and conditions and start a warranty replacement process online please go to Seagate Warranty and Replacements:
http://www.seagate.com/support/warranty-and-replacements/.

Otherwise, we can place the order of warranty replacement for you, for this please verify and fill-in the following information:
Full name (first name and last name)
Phone number for contact
Valid email address
Name of the company (if applicable)
Address, city, postal code, state/Province
Is the address residential or commercial?
Full billing address (only if it’s different from the shipping address)
Would you like to receive updates via SMS?
Do you agree with terms and conditions (see below)?

Once the replacement request is finished, you will receive an email with instructions for the replacement. Please read them carefully and follow them.
And when drive has been sent to our warehouse, keep the tracking number of the package and if there is any problem or question regarding the replacement process then please reply to this email attaching the tracking number and the RMA number.

For the replacement order you agree to:
Your continuance of the product return process means that you do agree to the Seagate privacy policy and Seagate online terms and conditions available at the legal page on our website at www.seagate.com, and to the return policy, shipping instructions, and E-Commerce & RMA Terms available on the support pages of our website. You will be provided an order acknowledgment following submission of your order, giving you the ability to review the entirety of these online terms and contact Seagate if you have any questions, or cancel your order if you do not agree to these terms.
Your continuance of the product return process means that you do agree that Seagate products may not be exported to restricted countries, or used to develop, produce or distribute chemical, biological, or nuclear weapons, missile technologies, or other terrorist activities.
Please be advised, Seagate may replace your product with a product that was previously used, repaired, and tested to meet Seagate specifications.
```
---

HTML code:

```
<div>Analyzing your information it seems that your drive has symptoms of failure. Unusual noises (such as clicks, grinding, whining, etc) are the most common indicators of physical drive failure, especially if the drive has started malfunctioning or causing logical errors. Please keep your drive disconnected from the computer to prevent further damage to the disk or the information stored in it.</div>
<div>&nbsp;</div>
<div>Important: Before proceeding with the warranty replacement order, be aware that data recovery is not covered under the warranty and by proceeding directly to the warranty replacement the data in the hard drive will be lost. For checking and reviewing advanced data recovery options please reply to this email.</div>
<div><br />After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}. We will be happy to replace your drive and we're sorry for all the trouble this may have caused.</div>
<div>To check terms and conditions and start a warranty replacement process online please go to <a href="http://www.seagate.com/support/warranty-and-replacements/">Seagate Warranty and Replacements</a>.</div>
<div>&nbsp;</div>
<div>Otherwise, we can place the order of warranty replacement for you, for this please verify and fill-in the following information:</div>
<ul>
<li>Full name (first name and last name)</li>
<li>Phone number for contact</li>
<li>Valid email address</li>
<li>Name of the company (if applicable)</li>
<li>Address, city, postal code, state/Province</li>
<li>Is the address residential or commercial?</li>
<li>Full billing address (only if it&rsquo;s different from the shipping address)</li>
<li>Would you like to receive updates via SMS?</li>
<li>Do you agree with terms and conditions (see below)?</li>
</ul>
<div>&nbsp;</div>
<div>Once the replacement request is finished, you will receive an email with instructions for the replacement. Please read them carefully and follow them.<br />And when drive has been sent to our warehouse, keep the tracking number of the package and if there is any problem or question regarding the replacement process then please reply to this email attaching the tracking number and the RMA number.</div>
<div>&nbsp;</div>
<div>For the replacement order you agree to:</div>
<div>Your continuance of the product return process means that you do agree to the Seagate privacy policy and Seagate online terms and conditions available at the legal page on our website at&nbsp;www.seagate.com, and to the return policy, shipping instructions, and E-Commerce &amp; RMA Terms available on the support pages of our website. You will be provided an order acknowledgment following submission of your order, giving you the ability to review the entirety of these online terms and contact Seagate if you have any questions, or cancel your order if you do not agree to these terms.<br />Your continuance of the product return process means that you do agree that Seagate products may not be exported to restricted countries, or used to develop, produce or distribute chemical, biological, or nuclear weapons, missile technologies, or other terrorist activities.<br />Please be advised, Seagate may replace your product with a product that was previously used, repaired, and tested to meet Seagate specifications.</div>
```
---

Speed note:

Composed an email to the customer reviewing the hardware damage, warranty status and including links to warranty and replacements. Including also information to request for RMA and ECOMM1, ITA scripts.
