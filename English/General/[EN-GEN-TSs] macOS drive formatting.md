### [EN-GEN-TSs] macOS drive formatting

Overview:

Instructions formatting for macOS, it implicates that the drive is going only to be used with macOS.

---

Preview:

<p>We are going to try to reformat the hard drive and check if the settings for formatting are correct:<br /><em>Please be aware that formatting the drive with Disk Utility will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you. Do you understand?​ ​</em></p>
<ol>
<li>Open Disk Utility: Open Finder &gt; Applications &gt; Utilities &gt; Disk Utility</li>
<li>Select the LaCie hard drive in the left panel.<br />Note: If you do not see drive, then click on the view button in the upper left-hand side, change to Show All Devices.</li>
<li>Select Erase, will open a new window with options:
<ul>
<li>Rename the drive (optional).</li>
<li>Select OS X Extended (Journaled) for Format.</li>
<li>Select GUID Partition Map for Scheme.<br />Note: If you do not see the scheme option, then you have selected the volume and not the drive. Click on the view button in the upper left-hand side, change to Show All Devices.</li>
</ul>
</li>
<li>Select Erase again, then Disk Utility will start the format. Once complete click in done.</li>
</ol>
<p>You can check the instructions for formatting with more detail in this <a href="https://www.seagate.com/support/kb/how-to-format-your-drive-in-macos-1011-and-above-007736en/">support page from Seagate</a>.</p>

---

Plain text:

```
We are going to try to reformat the hard drive and check if the settings for formatting are correct:
Please be aware that formatting the drive with Disk Utility will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you. Do you understand?​ ​
1. Open Disk Utility: Open Finder > Applications > Utilities > Disk Utility
2. Select the LaCie hard drive in the left panel.
Note: If you do not see drive, then click on the view button in the upper left-hand side, change to Show All Devices.
3. Select Erase, will open a new window with options:
Rename the drive (optional).
Select OS X Extended (Journaled) for Format.
Select GUID Partition Map for Scheme.
Note: If you do not see the scheme option, then you have selected the volume and not the drive. Click on the view button in the upper left-hand side, change to Show All Devices.
4. Select Erase again, then Disk Utility will start the format. Once complete click in done.
You can check the instructions for formatting with more detail in this support page from Seagate:
https://www.seagate.com/support/kb/how-to-format-your-drive-in-macos-1011-and-above-007736en/
```
---

HTML code:

```
<p>We are going to try to reformat the hard drive and check if the settings for formatting are correct:<br /><em>Please be aware that formatting the drive with Disk Utility will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you. Do you understand?​ ​</em></p>
<ol>
<li>Open Disk Utility: Open Finder &gt; Applications &gt; Utilities &gt; Disk Utility</li>
<li>Select the LaCie hard drive in the left panel.<br />Note: If you do not see drive, then click on the view button in the upper left-hand side, change to Show All Devices.</li>
<li>Select Erase, will open a new window with options:
<ul>
<li>Rename the drive (optional).</li>
<li>Select OS X Extended (Journaled) for Format.</li>
<li>Select GUID Partition Map for Scheme.<br />Note: If you do not see the scheme option, then you have selected the volume and not the drive. Click on the view button in the upper left-hand side, change to Show All Devices.</li>
</ul>
</li>
<li>Select Erase again, then Disk Utility will start the format. Once complete click in done.</li>
</ol>
<p>You can check the instructions for formatting with more detail in this <a href="https://www.seagate.com/support/kb/how-to-format-your-drive-in-macos-1011-and-above-007736en/">support page from Seagate</a>.</p>
```
---

Speed note:
Included instructions to formatting the hard drive in macOS, including a link for more details and data loss script.
