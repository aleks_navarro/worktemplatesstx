### [EN-GEN-CSs] COD


Overview:

Use this template when a Certify of Destruction, COD, is going to be provided to the customer. Follow the rules and policies in the wiki.

---

Preview:

<p>I understand that, due to the sensitive nature of your data, you would like to remove the platters before returning the drive(s) to us. You have been given authorization for a Certificate of Destruction (COD, attached to this email). Please fill out the attached COD form and then reply directly to this email with the completed form (scan it or take a photo of the COD document, attach to the reply email, and keep the document because it will be needed for the replacement order). You will need to send the top plate face (silver metal piece with a Seagate sticker) and the circuit board (the green board) of the drive(s). If this is an external drive, you will also need to send the external enclosure. This COD gives you permission to remove and destroy the platters.</p>
<p>Also, to start the process of replacement as soon as possible please reply to this email with the following information.</p>
<ul>
<li>Full Name</li>
<li>Email Address</li>
<li>Telephone Number</li>
<li>Physical Shipping Address</li>
<li>Serial Number(s) of the Drive(s)</li>
<li>Full billing address (only if it&rsquo;s different from the shipping address).</li>
</ul>
<p>For the replacement order with COD included, you agree to:<br />You are responsible for the <a href="https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/">postage and packaging</a> to our warehouse, and we will ship the replacement at our cost. <br />For the warranty replacement process, it is only necessary to send the defective hard drive. It is not required to include accessories such as USB cables, power supplies or documentation that came with the drive.<br />Please note that per your Limited Warranty Policy, Seagate may replace your product with a product that was previously used, repaired and tested to meet Seagate specifications. You can check more about terms and conditions in the following links:</p>
<ul>
<li><a href="https://www.seagate.com/support/warranty-and-replacements/limited-consumer-warranty/">Seagate Limited Warranty</a></li>
<li><a href="https://www.seagate.com/support/warranty-and-replacements/ecommerce-terms/">Seagate E-Commerce and RMA Terms.</a></li>
</ul>

---

Plain text:

```
I understand that, due to the sensitive nature of your data, you would like to remove the platters before returning the drive(s) to us. You have been given authorization for a Certificate of Destruction (COD, attached to this email). Please fill out the attached COD form and then reply directly to this email with the completed form (scan it or take a photo of the COD document, attach to the reply email, and keep the document because it will be needed for the replacement order). You will need to send the top plate face (silver metal piece with a Seagate sticker) and the circuit board (the green board) of the drive(s). If this is an external drive, you will also need to send the external enclosure. This COD gives you permission to remove and destroy the platters.

Also, to start the process of replacement as soon as possible please reply to this email with the following information.
Full Name
Email Address
Telephone Number
Physical Shipping Address
Serial Number(s) of the Drive(s)
Full billing address (only if it’s different from the shipping address).

For the replacement order with COD included, you agree to:
You are responsible for the postage and packaging to our warehouse, and we will ship the replacement at our cost. Guidelines regarding the packaging of the drive can be found here:
https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/.
For the warranty replacement process, it is only necessary to send the defective hard drive. It is not required to include accessories such as USB cables, power supplies or documentation that came with the drive.
Please note that per your Limited Warranty Policy, Seagate may replace your product with a product that was previously used, repaired and tested to meet Seagate specifications. You can check more about terms and conditions in the following links:
Seagate Limited Warranty
https://www.seagate.com/support/warranty-and-replacements/limited-consumer-warranty/.
Seagate E-Commerce and RMA Terms
https://www.seagate.com/support/warranty-and-replacements/ecommerce-terms/.
```
---

HTML code:

```
<p>I understand that, due to the sensitive nature of your data, you would like to remove the platters before returning the drive(s) to us. You have been given authorization for a Certificate of Destruction (COD, attached to this email). Please fill out the attached COD form and then reply directly to this email with the completed form (scan it or take a photo of the COD document, attach to the reply email, and keep the document because it will be needed for the replacement order). You will need to send the top plate face (silver metal piece with a Seagate sticker) and the circuit board (the green board) of the drive(s). If this is an external drive, you will also need to send the external enclosure. This COD gives you permission to remove and destroy the platters.</p>
<p>Also, to start the process of replacement as soon as possible please reply to this email with the following information.</p>
<ul>
<li>Full Name</li>
<li>Email Address</li>
<li>Telephone Number</li>
<li>Physical Shipping Address</li>
<li>Serial Number(s) of the Drive(s)</li>
<li>Full billing address (only if it&rsquo;s different from the shipping address).</li>
</ul>
<p>For the replacement order with COD included, you agree to:<br />You are responsible for the <a href="https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/">postage and packaging</a> to our warehouse, and we will ship the replacement at our cost. <br />For the warranty replacement process, it is only necessary to send the defective hard drive. It is not required to include accessories such as USB cables, power supplies or documentation that came with the drive.<br />Please note that per your Limited Warranty Policy, Seagate may replace your product with a product that was previously used, repaired and tested to meet Seagate specifications. You can check more about terms and conditions in the following links:</p>
<ul>
<li><a href="https://www.seagate.com/support/warranty-and-replacements/limited-consumer-warranty/">Seagate Limited Warranty</a></li>
<li><a href="https://www.seagate.com/support/warranty-and-replacements/ecommerce-terms/">Seagate E-Commerce and RMA Terms.</a></li>
</ul>
```
---

Speed note:
Explained the policies of COD, requesting the necessary information for the placement of the RMA with COD. Included links to Seagate for packaging and instructions, limited consumer warranty and ecommerce policies.
