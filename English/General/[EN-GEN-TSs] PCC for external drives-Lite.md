### [EN-GEN-TSs] PCC for external drives-Lite

Overview:

This is a template which resumes in one link from Seagate Support a PCC for external drives. Suggested to be used along another troubleshooter, like SeaTools, to avoid a wall of text.

---

Preview:

<p>To review and check another possible issues with your device please try following the <a href="https://www.seagate.com/support/kb/usb-external-troubleshooter-003581en">troubleshooting guide for external drives</a>.<br />Please reply to this email with the results of the troubleshooting.</p>

---

Plain text:

```
To review and check another possible issues with your device please try following the troubleshooting guide for external drives:
https://www.seagate.com/support/kb/usb-external-troubleshooter-003581en
Please reply to this email with the results of the troubleshooting.
```
---

HTML code:

```
<p>To review and check another possible issues with your device please try following the <a href="https://www.seagate.com/support/kb/usb-external-troubleshooter-003581en">troubleshooting guide for external drives</a>.<br />Please reply to this email with the results of the troubleshooting.</p>
```
---

Speed note:

Included a link to Seagate Support for External USB Troubleshooter.
