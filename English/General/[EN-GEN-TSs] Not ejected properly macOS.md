### [EN-GEN-TSs] Not ejected properly macOS

Overview:

Use this template when there are issues with not ejected properly messages in macOS. 

---

Preview:

<p>Usually this problem is presented due to hardware or operating system configurations that interfere with the drive functioning.&nbsp;<br />Let's try to review this guide from <a href="https://www.seagate.com/support/kb/disk-not-ejected-properly-on-mac/">Seagate Support</a>. Check the suggested by this guide, specially be sure to review the energy settings mentioned in the guide.<br />Also, if the mentioned guide did not help let's try to check the following:</p>
<ul>
<li>Try to connect and recreate the drive issue with another computer, try another cable also.</li>
<li>Contact to <a href="support.apple.com">Apple Support</a> to review another possible issues with hardware or software.</li>
</ul>
<p><em>Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.</em></p>

---

Plain text:

```
Usually this problem is presented due to hardware or operating system configurations that interfere with the drive functioning. 
Let's try to review this guide from Seagate Support:
https://www.seagate.com/support/kb/disk-not-ejected-properly-on-mac/.
Check the suggested by this guide, specially be sure to review the energy settings mentioned in the guide.
Also, if the mentioned guide did not help let's try to check the following:
Try to connect and recreate the drive issue with another computer, try another cable also.
Contact to Apple Support to review another possible issues with hardware or software: 
support.apple.com.
Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.
```
---

HTML code:

```
<p>Usually this problem is presented due to hardware or operating system configurations that interfere with the drive functioning.&nbsp;<br />Let's try to review this guide from <a href="https://www.seagate.com/support/kb/disk-not-ejected-properly-on-mac/">Seagate Support</a>. Check the suggested by this guide, specially be sure to review the energy settings mentioned in the guide.<br />Also, if the mentioned guide did not help let's try to check the following:</p>
<ul>
<li>Try to connect and recreate&nbsp;the drive issue with another computer, try another cable also.</li>
<li>Contact to <a href="support.apple.com">Apple Support</a> to review another possible issues with hardware or software.</li>
</ul>
<p><em>Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.</em></p>
```
---

Speed note:

Explaining that usually this is an issue related to power settings. Included link for guide for TS from LaCie, suggested checking with computer and cable and Apple support for another possible hardware or software issues, included link with third party url script.
