### [EN-GEN-TSs] Windows drive formatting

Overview:

Instructions formatting for Windows, it implicates that the drive is going only to be used with Windows.

---

Preview:

<p>We are going to try to reformat the hard drive by using Disk Management in Windows and check if the settings for formatting are correct:<br /><em>Please be aware that formatting the drive with Disk Management will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you. Do you understand?​ ​</em></p>
<ol>
<li>To open the Disk Management press and hold the Windows key + R</li>
<li>From the list of storage devices in the middle of the Disk Management window, locate your Seagate device.</li>
<li>The partition must be available to format. If it is currently formatted, right-click on the partition and then choose Delete.</li>
<li>To create a new partition, right-click on the volume and select New Simple Volume. Follow the on-screen instructions when the New Simple Volume Wizard appears. Rename your drive, choose NTFS as File System and choose GPT and partition scheme.</li>
<li>Click OK. You will be asked to confirm. After confirmation, your drive will begin formatting. The length of time required depends on the size of the capacity.</li>
</ol>
<p>You can check the instructions for formatting with more detail in this <a href="https://www.seagate.com/support/kb/how-to-format-your-drive-on-windows/">support page from Seagate</a>.</p>


---

Plain text:

```
We are going to try to reformat the hard drive by using Disk Management in Windows and check if the settings for formatting are correct:
Please be aware that formatting the drive with Disk Management will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you. Do you understand?​ ​
To open the Disk Management press and hold the Windows key + R
From the list of storage devices in the middle of the Disk Management window, locate your Seagate device.
The partition must be available to format. If it is currently formatted, right-click on the partition and then choose Delete.
To create a new partition, right-click on the volume and select New Simple Volume. Follow the on-screen instructions when the New Simple Volume Wizard appears. Rename your drive, choose NTFS as File System and choose GPT and partition scheme.
Click OK. You will be asked to confirm. After confirmation, your drive will begin formatting. The length of time required depends on the size of the capacity.

You can check the instructions for formatting with more detail in this support page from Seagate:
https://www.seagate.com/support/kb/how-to-format-your-drive-on-windows/.
```
---

HTML code:

```
<p>We are going to try to reformat the hard drive by using Disk Management in Windows and check if the settings for formatting are correct:<br /><em>Please be aware that formatting the drive with Disk Management will permanently destroy your important data (pictures, videos, music and documents), which cannot be recovered without cost to you. Do you understand?​ ​</em></p>
<ol>
<li>To open the Disk Management press and hold the Windows key + R</li>
<li>From the list of storage devices in the middle of the Disk Management window, locate your Seagate device.</li>
<li>The partition must be available to format. If it is currently formatted, right-click on the partition and then choose Delete.</li>
<li>To create a new partition, right-click on the volume and select New Simple Volume. Follow the on-screen instructions when the New Simple Volume Wizard appears. Rename your drive, choose NTFS as File System and choose GPT and partition scheme.</li>
<li>Click OK. You will be asked to confirm. After confirmation, your drive will begin formatting. The length of time required depends on the size of the capacity.</li>
</ol>
<p>You can check the instructions for formatting with more detail in this <a href="https://www.seagate.com/support/kb/how-to-format-your-drive-on-windows/">support page from Seagate</a>.</p>
```
---

Speed note:
Included instructions to formatting the hard drive in Windows with Disk Management, including a link for more details and data loss script.
