### [EN-GEN-TSs] Time Machine in macOS - Lite

Overview:

One link with instructions to Apple Support for instructions to set up Time Machine. See also: Time Machine in macOS, Toolkit Resources.

---

Preview:

<p>It is possible to set up a backup in Mac using Time Machine, which is an incorporated software to macOS. You can find more info about <a href="https://support.apple.com/en-us/HT201250">Time Machine in Apple Support</a>.<br /><em>Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.</em></p>

---

Plain text:

```
It is possible to set up a backup in Mac using Time Machine, which is an incorporated software to macOS. You can find more info about Time Machine in Apple Support:
https://support.apple.com/en-us/HT201250.
Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.
```
---

HTML code:

```
<p>It is possible to set up a backup in Mac using Time Machine, which is an incorporated software to macOS. You can find more info about <a href="https://support.apple.com/en-us/HT201250">Time Machine in Apple Support</a>.<br /><em>Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.</em></p>
```
---

Speed note:

Including a link to Apple Support for information about Time Machine, with third party URL script.
