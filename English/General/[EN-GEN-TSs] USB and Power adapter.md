### [EN-GEN-TSs] USB and Power adapter

Overview:

Use this template to provide the necessary information for general USB and Power supply details. Works for both accessories.

---

Preview:

<p>We at Seagate/LaCie, don&rsquo;t make reparations of drives or provide accessories, such as power supplies or USB cables, or hardware spare parts. Fortunately, you can easily purchase replacement cables for your drive, all you need is a USB 3 micro B cable and power supply for your external hard drive. You can find these at any local electronic store.</p>
<p>The drives were designed to use standard USB 3 cables and universal power supplies so we do not have a specific brand or store we recommend for this. For a portable drive, you will want a cable that is 18 inches or shorter. For a desktop external drive, you will want a cable that is 3 feet long or shorter. A cable that is longer, can lead to connection issues.</p>
<p>Here are some websites where you can see about purchasing a replacement USB 3 cable:</p>
<ul>
<li><a href="https://www.amazon.com/">Amazon</a></li>
<li><a href="http://www.newegg.com/">NewEgg</a></li>
<li><a href="http://www.microcenter.com/">MicroCenter</a></li>
</ul>
<p>The specifications about what type of power supply you need are:</p>
<ul>
<li>Electrical specs: 12-Volt 2-Amp Output</li>
<li>Connection type: Outside 5.5mm Inside 2.5mm (Positive Center)</li>
<li>Model number:</li>
</ul>
<p>As long as it meets those specs the power supply will work on your hard drive. Here are some websites where you can see about purchasing a replacement power supply:</p>
<ul>
<li><a href="http://www.12vadapters.com/">12V Adapters</a></li>
<li><a href="http://www.markertek.com/">Markertek</a></li>
</ul>
<p><em>Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.</em></p>

---

Plain text:

```
We at Seagate/LaCie, don’t make reparations of drives or provide accessories, such as power supplies or USB cables, or hardware spare parts. Fortunately, you can easily purchase replacement cables for your drive, all you need is a USB 3 micro B cable and power supply for your external hard drive. You can find these at any local electronic store.

The drives were designed to use standard USB 3 cables and universal power supplies so we do not have a specific brand or store we recommend for this. For a portable drive, you will want a cable that is 18 inches or shorter. For a desktop external drive, you will want a cable that is 3 feet long or shorter. A cable that is longer, can lead to connection issues.

Here are some websites where you can see about purchasing a replacement USB 3 cable:
-https://www.amazon.com/
-http://www.newegg.com/
-http://www.microcenter.com/

The specifications about what type of power supply you need are
Power Supply: 12-Volt 2-Amp Output
Power Supply Barrel: Outside 5.5mm Inside 2.5mm (Positive Center)

As long as it meets those specs the power supply will work on your hard drive. Here are some websites where you can see about purchasing a replacement power supply:

-http://www.12vadapters.com/
-https://www.amazon.com/
-http://www.markertek.com/
Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.
```
---

HTML code:

```
<p>We at Seagate/LaCie, don&rsquo;t make reparations of drives or provide accessories, such as power supplies or USB cables, or hardware spare parts. Fortunately, you can easily purchase replacement cables for your drive, all you need is a USB 3 micro B cable and power supply for your external hard drive. You can find these at any local electronic store.</p>
<p>The drives were designed to use standard USB 3 cables and universal power supplies so we do not have a specific brand or store we recommend for this. For a portable drive, you will want a cable that is 18 inches or shorter. For a desktop external drive, you will want a cable that is 3 feet long or shorter. A cable that is longer, can lead to connection issues.</p>
<p>Here are some websites where you can see about purchasing a replacement USB 3 cable:</p>
<ul>
<li><a href="https://www.amazon.com/">Amazon</a></li>
<li><a href="http://www.newegg.com/">NewEgg</a></li>
<li><a href="http://www.microcenter.com/">MicroCenter</a></li>
</ul>
The specifications about what type of power supply you need are:
<ul>
<li>Electrical specs: 12-Volt 2-Amp Output</li>
<li>Connection type: Outside 5.5mm Inside 2.5mm (Positive Center)</li>
<li>Model number:</li>
</ul>
<p>As long as it meets those specs the power supply will work on your hard drive. Here are some websites where you can see about purchasing a replacement power supply:</p>
<ul>
<li><a href="http://www.12vadapters.com/">12V Adapters</a></li>
<li><a href="http://www.markertek.com/">Markertek</a></li>
</ul>
<p><em>Please be aware that the provided URL(s) is not a Seagate/LaCie maintained or monitored website.</em></p>
```
---

Speed note:

Included the policy regarding the accessories, but including details about the USB cable used and power supply specs. Included links to where to find, with third party url script.
