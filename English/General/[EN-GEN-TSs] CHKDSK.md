### [EN-GEN-TSs] CHKDSK

Overview:

This template provides a five step tutorial on how to use CHKDSK to attempt to repair a hard drive, useful for drive corruption and failed checksums.

---

Preview:

<p>We are going to check and attempt to repair bad sector with a Windows tool named Check Disk (ChkDsk).. Always backup your data before running any check disk (chkdsk) scans of your hard drive. If there is a bad sector, any data that may have been accessible before chkdsk, will likely become inaccessible once the bad sector is re-allocated.</p>
<ol>
<li>Press "Windows" Key and type cmd, then do right-click "Command Prompt" and choose 'Run as administrator' option.</li>
<li>Click "Yes" when you are presented with a window requesting permission to launch the Command Prompt as Administrator.</li>
<li>In the new Command Prompt window, type: chkdsk E: /f /r /x (replace E with the drive letter for your device).<br />This command will attempt to fix any file system error. The length of time for this to complete will vary depending on the capacity of the drive.<br /><em>Please be aware that the use of CHKDSK could result in the inability to access your important data (pictures, videos, music and documents) stored in the drive, which cannot be recovered without cost to you.</em></li>
</ol>


---

Plain text:

```
We are going to check and attempt to repair bad sector with a Windows tool named Check Disk (ChkDsk).. Always backup your data before running any check disk (chkdsk) scans of your hard drive. If there is a bad sector, any data that may have been accessible before chkdsk, will likely become inaccessible once the bad sector is re-allocated.
1. Press "Windows" Key and type cmd, then do right-click "Command Prompt" and choose 'Run as administrator' option.
2. Click "Yes" when you are presented with a window requesting permission to launch the Command Prompt as Administrator.
3. In the new Command Prompt window, type: chkdsk E: /f /r /x (replace E with the drive letter for your device).
This command will attempt to fix any file system error. The length of time for this to complete will vary depending on the capacity of the drive.
Please be aware that the use of CHKDSK could result in the inability to access your important data (pictures, videos, music and documents) stored in the drive, which cannot be recovered without cost to you.
```
---

HTML code:

```
<p>We are going to check and attempt to repair bad sector with a Windows tool named Check Disk (ChkDsk).. Always backup your data before running any check disk (chkdsk) scans of your hard drive. If there is a bad sector, any data that may have been accessible before chkdsk, will likely become inaccessible once the bad sector is re-allocated.</p>
<ol>
<li>Press "Windows" Key and type cmd, then do right-click "Command Prompt" and choose 'Run as administrator' option.</li>
<li>Click "Yes" when you are presented with a window requesting permission to launch the Command Prompt as Administrator.</li>
<li>In the new Command Prompt window, type: chkdsk E: /f /r /x (replace E with the drive letter for your device).<br />This command will attempt to fix any file system error. The length of time for this to complete will vary depending on the capacity of the drive.<br /><em>Please be aware that the use of CHKDSK could result in the inability to access your important data (pictures, videos, music and documents) stored in the drive, which cannot be recovered without cost to you.</em></li>
</ol>
```
---

Speed note:

Provided instructions to use the command CHKDSK to attempt to repair the hard drive, included possible data loss script.
