### [EN-LAC-CSs] RMA-Online Lite

Overview:

Small review of the warranty replacement and link to warranty and replacements. Use it to avoid wall texts.

---

Preview:

<p>After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}.<br />To review the warranty of the device, check terms and conditions and start a warranty replacement process online please go to <a href="https://www.lacie.com/support/warranty/">LaCie Warranty and Replacements</a>.</p>

---

Plain text:

```
After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}.
To review the warranty of the device, check terms and conditions and start a warranty replacement process online please go to LaCie Warranty and Replacements:
https://www.lacie.com/support/warranty/.

```
---

HTML code:

```
<p>After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}.<br />To review the warranty of the device, check terms and conditions and start a warranty replacement process online please go to <a href="https://www.lacie.com/support/warranty/">LaCie Warranty and Replacements</a>.</p>
```
---

Speed note:

Reviewed warranty of device and including link to warranty and replacements.
