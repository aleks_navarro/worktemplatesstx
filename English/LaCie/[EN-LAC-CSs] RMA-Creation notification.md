### [EN-LAC-CSs] RMA-Creation notification

Overview:

Text to notify the customer that an RMA has been processed. It is required to process the order in GRS before using and sending the email, since this template requires of the RMA number. See also: RMA-Email Request, RMA-Online Instructions, RMA-Hybrid instructions.

---

Preview:

<p>A standard RMA has been processed for your drive, ##drvnme## with serial number {!Case.Serial_Number__c}. The RMA number is ##rmanum##. You will receive an email confirmation within an hour that will contain your RMA number, the return address, and the packing and shipping guidelines. Please reply to this email or call us if you do not receive such an email in the next hour. If you do not accept any of the terms contained in that email, please call us back within the next 3 hours, and we will cancel your order.</p>
<p>Please return your drive within 30 days, so the order does not expire. It is also recommended obtaining the tracking number for your records. <br />You are responsible for the <a href="https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/">postage and packaging</a> to our warehouse, and we will ship the replacement at our cost. For the warranty replacement process, it is only necessary to send the defective drive, it is not required to include accessories such as USB cables, power supplies or documentation that came with the drive.</p>
<p>Reply to this email if there are problems regarding the warranty replacement, including the tracking number of the package with the faulty drive and the RMA number.</p>
<p>For more information please check <a href="https://www.seagate.com/support/warranty-and-replacements/">Seagate Warranty and Replacements</a>.</p>

---

Plain text:

```
A standard RMA has been processed for your drive, ##drvnme## with serial number {!Case.Serial_Number__c}. The RMA number is ##rmanum##. You will receive an email confirmation within an hour that will contain your RMA number, the return address, and the packing and shipping guidelines. Please reply to this email or call us if you do not receive such an email in the next hour. If you do not accept any of the terms contained in that email, please call us back within the next 3 hours, and we will cancel your order.

Please return your drive within 30 days, so the order does not expire. It is also recommended obtaining the tracking number for your records.
You are responsible for the postage and packaging to our warehouse, and we will ship the replacement at our cost. Check instructions for shipping in the following link:
https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/
For the warranty replacement process, it is only necessary to send the defective drive, it is not required to include accessories such as USB cables, power supplies or documentation that came with the drive.

Reply to this email if there are problems regarding the warranty replacement, including the tracking number of the package with the faulty drive and the RMA number.

For more information please check Seagate Warranty and Replacements:
https://www.seagate.com/support/warranty-and-replacements/

```
---

HTML code:

```
<p>A standard RMA has been processed for your drive, ##drvnme## with serial number {!Case.Serial_Number__c}. The RMA number is ##rmanum##. You will receive an email confirmation within an hour that will contain your RMA number, the return address, and the packing and shipping guidelines. Please reply to this email or call us if you do not receive such an email in the next hour. If you do not accept any of the terms contained in that email, please call us back within the next 3 hours, and we will cancel your order.</p>
<p>Please return your drive within 30 days, so the order does not expire. It is also recommended obtaining the tracking number for your records. <br />You are responsible for the <a href="https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/">postage and packaging</a> to our warehouse, and we will ship the replacement at our cost. For the warranty replacement process, it is only necessary to send the defective drive, it is not required to include accessories such as USB cables, power supplies or documentation that came with the drive.</p>
<p>Reply to this email if there are problems regarding the warranty replacement, including the tracking number of the package with the faulty drive and the RMA number.</p>
<p>For more information please check <a href="https://www.seagate.com/support/warranty-and-replacements/">Seagate Warranty and Replacements</a>.</p>
```
---

Speed note:

Notified about the RMA creation, including basic drive information and RMA number. Including further instructions for postage and packaging and where to check more information, with links to Seagate Support.
