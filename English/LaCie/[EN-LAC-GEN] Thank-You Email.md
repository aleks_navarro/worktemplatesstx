### [EN-LAC-GEN] Thank-You Email

Overview:

When a thank-you email is received, use this template for replying the thanks.

---

Preview:

<p>Hello {!Contact.FirstName} {!Contact.LastName},</p>
<p>This is {!Case.OwnerFirstName} from LaCie Support. We&rsquo;re happy to hear that we were able to assist you in resolving your issue ##issdcp##.<br />Reply to this email if any problem or concern appears regarding this ##issdcp##.<br />However, please do not hesitate in opening another support ticket if you have any more issues or inquiries regarding Seagate products and services, we will be happy to assist you.</p>
<p>I&rsquo;d like to inform you that you will be receiving an email regarding the support I have provided in the last days. I would appreciate it if you take the time to fill out the survey.</p>
<p>This technical support case will be closed from now (case number {!Case.CaseNumber}).</p>
<p>Thank you for choosing LaCie for your digital storage needs.</p>
<p>Regards,<br />{!Case.OwnerFirstName}<br />LaCie Support</p>

---

Plain text:

```
Hello {!Contact.FirstName} {!Contact.LastName},

This is {!Case.OwnerFirstName} from LaCie Support. We’re happy to hear that we were able to assist you in resolving your issue ##issdcp##.
Reply to this email if any problem or concern appears regarding this ##issdcp##.
However, please do not hesitate in opening another support ticket if you have any more issues or inquiries regarding Seagate products and services, we will be happy to assist you.

I’d like to inform you that you will be receiving an email regarding the support I have provided in the last days. I would appreciate it if you take the time to fill out the survey.

This technical support case will be closed from now (case number {!Case.CaseNumber}).

​​​​​​​Thank you for choosing LaCie for your digital storage needs.

Regards,
{!Case.OwnerFirstName}
LaCie Support
```
---

HTML code:

```
<p>Hello {!Contact.FirstName} {!Contact.LastName},</p>
<p>This is {!Case.OwnerFirstName} from LaCie Support. We&rsquo;re happy to hear that we were able to assist you in resolving your issue ##issdcp##.<br />Reply to this email if any problem or concern appears regarding this ##issdcp##.<br />However, please do not hesitate in opening another support ticket if you have any more issues or inquiries regarding Seagate products and services, we will be happy to assist you.</p>
<p>I&rsquo;d like to inform you that you will be receiving an email regarding the support I have provided in the last days. I would appreciate it if you take the time to fill out the survey.</p>
<p>This technical support case will be closed from now (case number {!Case.CaseNumber}).</p>
<p>Thank you for choosing LaCie for your digital storage needs.</p>
<p>Regards,<br />{!Case.OwnerFirstName}<br />LaCie Support</p>
```
---

Speed note:

Composed a thank-you email to the customer requesting to reply the email if there are more issues or take a new ticket, announcing case closure and customer survey.
