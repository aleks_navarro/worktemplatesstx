### [EN-LAC-CSs] RMA-Online Instructions

Overview:

This template explains the way to create a RMA via Seagate Warranty and Replacements.

---

Preview:

<p>Analyzing the provided information it seems that your drive has symptoms of failure. Unusual noises (such as clicks, grinding, whining, etc) are the most common indicators of a physical drive failure, especially if the drive has started malfunctioning or causing logical errors. Please keep your drive disconnected from the computer to prevent further damage to the disk or the information stored in it.<br />After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}. We will be happy to replace your drive and I'm sorry for all the trouble this may have caused.</p>
<p>However, we can make for this occasion a warranty extension for your drive to get replaced. Please be aware that this is a one-time extraordinary case in which Seagate can assist because we care about the satisfaction of our customers.</p>
<p>Important: Before proceeding with the warranty replacement order, be aware that data recovery is not covered under the warranty and by proceeding directly to the warranty replacement the data in the hard drive will be lost. LaCie works with Seagate in data recovery. For checking and reviewing advanced data recovery options please reply to this email.</p>
<p>Otherwise, if data recovery is not necessary, then to process the replacement online and to review Seagate&rsquo;s return policy, E-Commerce &amp; RMA Terms and shipping instructions please go to <a href="https://www.lacie.com/support/warranty/">LaCie Warranty and Replacements&nbsp;</a>and follow the next instructions:</p>
<ol>
<li>Scroll down to the section &ldquo;Returns and Replacements&rdquo;.</li>
<li>Select "Register" to create an account if you do not have one already or Select "Log In" to use an existing Seagate account.</li>
<li>Go to Create Order and enter the Product and Shipping details. Click "Add More Products" if you want to create RMA for more than one product.</li>
<li>Click "Manage address" if you want to change/edit your address.</li>
<li>Enter the Captcha and click on Check Warranty Status using the following information:<br />
<ul>
<li>Serial Number: {!Case.Serial_Number__c}</li>
<li>Part Number: {!Case.Part_Number__c}</li>
<li>Or, Model Number: {!Case.Model_Number__c}</li>
</ul>
</li>
<li>The next page will display the warranty information on your drive. By default, you will be given a Standard RMA return option. Depending on availability, you may see additional Advanced Replacement options.</li>
<li>Continue through the prompts to complete your return. It is recommended to enable the notifications via SMS.</li>
</ol>
<p>General notes regard the warranty replacement order<br />Once the replacement request is finished, you will receive an email with instructions for the replacement. Please read them carefully and follow them.<br />Once the defective drive has been sent to our warehouse, keep the tracking number of the package and if there is any problem or question regarding the replacement process then please reply to this email attaching the tracking number and the RMA number.<br />You are responsible for the <a href="https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/">postage and packaging</a> to our warehouse, and we will ship the replacement at our cost.<br />For the warranty replacement process, it is only necessary to send the defective hard drive. It is not required to include accessories such as USB cables, power supplies or documentation that came with the drive.<br />Please note that per your Limited Warranty Policy, Seagate may replace your product with a product that was previously used, repaired and tested to meet Seagate specifications. You can check more about terms and conditions in the following links:<br /><a href="https://www.seagate.com/support/warranty-and-replacements/limited-consumer-warranty/">Seagate Limited Warranty</a>.<br /><a href="https://www.seagate.com/support/warranty-and-replacements/ecommerce-terms/">Seagate E-Commerce and RMA Terms</a>.</p>

---

Plain text:

```
Analyzing the provided information it seems that your drive has symptoms of failure. Unusual noises (such as clicks, grinding, whining, etc) are the most common indicators of a physical drive failure, especially if the drive has started malfunctioning or causing logical errors. Please keep your drive disconnected from the computer to prevent further damage to the disk or the information stored in it.
After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}. We will be happy to replace your drive and I'm sorry for all the trouble this may have caused.

However, we can make for this occasion a warranty extension for your drive to get replaced. Please be aware that this is a one-time extraordinary case in which Seagate can assist because we care about the satisfaction of our customers.

Important: Before proceeding with the warranty replacement order, be aware that data recovery is not covered under the warranty and by proceeding directly to the warranty replacement the data in the hard drive will be lost. LaCie works with Seagate in data recovery. For checking and reviewing advanced data recovery options please reply to this email.

Otherwise, if data recovery is not necessary, then to process the replacement online and to review Seagate’s return policy, E-Commerce & RMA Terms and shipping instructions please go to Seagate Warranty and Replacements
https://www.lacie.com/support/warranty/
and follow the next instructions:
1. Scroll down to the section “Returns and Replacements”
2. Select "Register" to create an account if you do not have one already or Select "Log In" to use an existing Seagate account
3. Go to Create Order and enter the Product and Shipping details. Click "Add More Products" if you want to create RMA for more than one product
4. Click "Manage address" if you want to change/edit your address
5. Enter the Captcha and click on Check Warranty Status
Serial Number: {!Case.Serial_Number__c}
Part Number: {!Case.Part_Number__c}
Or, Model Number: {!Case.Model_Number__c}
6. The next page will display the warranty information on your drive. By default, you will be given a Standard RMA return option. Depending on availability, you may see additional Advanced Replacement options.
7. Continue through the prompts to complete your return. It is recommended to enable the notifications via SMS.

General notes regard the warranty replacement order
Once the replacement request is finished, you will receive an email with instructions for the replacement. Please read them carefully and follow them.
Once the defective drive has been sent to our warehouse, keep the tracking number of the package and if there is any problem or question regarding the replacement process then please reply to this email attaching the tracking number and the RMA number.
You are responsible for the postage and packaging to our warehouse, and we will ship the replacement at our cost. Guidelines regarding the packaging of the drive can be found here:
https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/.
For the warranty replacement process, it is only necessary to send the defective hard drive. It is not required to include accessories such as USB cables, power supplies or documentation that came with the drive.
Please note that per your Limited Warranty Policy, Seagate may replace your product with a product that was previously used, repaired and tested to meet Seagate specifications. You can check more about terms and conditions in the following links:
Seagate Limited Warranty
https://www.seagate.com/support/warranty-and-replacements/limited-consumer-warranty/.
Seagate E-Commerce and RMA Terms
https://www.seagate.com/support/warranty-and-replacements/ecommerce-terms/.

```
---

HTML code:

```
<p>Analyzing the provided information it seems that your drive has symptoms of failure. Unusual noises (such as clicks, grinding, whining, etc) are the most common indicators of a physical drive failure, especially if the drive has started malfunctioning or causing logical errors. Please keep your drive disconnected from the computer to prevent further damage to the disk or the information stored in it.<br />After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}. We will be happy to replace your drive and I'm sorry for all the trouble this may have caused.</p>
<p>However, we can make for this occasion a warranty extension for your drive to get replaced. Please be aware that this is a one-time extraordinary case in which Seagate can assist because we care about the satisfaction of our customers.</p>
<p>Important: Before proceeding with the warranty replacement order, be aware that data recovery is not covered under the warranty and by proceeding directly to the warranty replacement the data in the hard drive will be lost. LaCie works with Seagate in data recovery. For checking and reviewing advanced data recovery options please reply to this email.</p>
<p>Otherwise, if data recovery is not necessary, then to process the replacement online and to review Seagate&rsquo;s return policy, E-Commerce &amp; RMA Terms and shipping instructions please go to <a href="https://www.lacie.com/support/warranty/">Seagate Warranty and Replacements&nbsp;</a>and follow the next instructions:</p>
<ol>
<li>Scroll down to the section &ldquo;Returns and Replacements&rdquo;.</li>
<li>Select "Register" to create an account if you do not have one already or Select "Log In" to use an existing Seagate account.</li>
<li>Go to Create Order and enter the Product and Shipping details. Click "Add More Products" if you want to create RMA for more than one product.</li>
<li>Click "Manage address" if you want to change/edit your address.</li>
<li>Enter the Captcha and click on Check Warranty Status using the following information:<br />
<ul>
<li>Serial Number: {!Case.Serial_Number__c}</li>
<li>Part Number: {!Case.Part_Number__c}</li>
<li>Or, Model Number: {!Case.Model_Number__c}</li>
</ul>
</li>
<li>The next page will display the warranty information on your drive. By default, you will be given a Standard RMA return option. Depending on availability, you may see additional Advanced Replacement options.</li>
<li>Continue through the prompts to complete your return. It is recommended to enable the notifications via SMS.</li>
</ol>
<p>General notes regard the warranty replacement order<br />Once the replacement request is finished, you will receive an email with instructions for the replacement. Please read them carefully and follow them.<br />Once the defective drive has been sent to our warehouse, keep the tracking number of the package and if there is any problem or question regarding the replacement process then please reply to this email attaching the tracking number and the RMA number.<br />You are responsible for the <a href="https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/">postage and packaging</a> to our warehouse, and we will ship the replacement at our cost.<br />For the warranty replacement process, it is only necessary to send the defective hard drive. It is not required to include accessories such as USB cables, power supplies or documentation that came with the drive.<br />Please note that per your Limited Warranty Policy, Seagate may replace your product with a product that was previously used, repaired and tested to meet Seagate specifications. You can check more about terms and conditions in the following links:<br /><a href="https://www.seagate.com/support/warranty-and-replacements/limited-consumer-warranty/">Seagate Limited Warranty</a>.<br /><a href="https://www.seagate.com/support/warranty-and-replacements/ecommerce-terms/">Seagate E-Commerce and RMA Terms</a>.</p>
```
---

Speed note:

Explaining that the hard drive has failed, confirming warranty status, and provided instructions for online RMA, Explaining data recovery conditions. Further instructions for RMA included, adding links to Seagate Limited Warranty, E-Commerce and RMA Terms.
