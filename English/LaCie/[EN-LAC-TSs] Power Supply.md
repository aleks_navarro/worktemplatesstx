### [EN-LAC-TSs] Power Supply

Overview:

When the customer is requesting for the power supply. If it is indicated that the device is new request for the POP to validate the 90 days of warranty in accessories after the purchase.

---

Preview:

<p>Unfortunately, we do not sell or provide power supplies separately; however, here is a link that shows LaCie&rsquo;s external drives&rsquo; <a href="https://www.lacie.com/support/kb/identifying-my-power-supply-007350en/">power supply specifications</a> and <a href="https://www.lacie.com/support/kb/identify-a-bad-power-supply-006242en/">how to identify a bad power supply</a>.</p>
<p>The specifications of the required power supply for your device are the following:</p>
<ul>
<li>Electrical specs:</li>
<li>Connector type:</li>
<li>Model number:</li>
</ul>
<p>As long as it meets those specs the power supply will work on your hard drive. The drives were designed to use universal power supplies, so we do not have a specific brand or store we recommend for this. For your convenience, here are some websites where you can see about purchasing a replacement power supply:</p>
<ul>
<li><a href="http://www.12vadapters.com/">12VAdapters</a></li>
<li><a href="https://www.amazon.com/">Amazon</a></li>
<li><a href="http://www.markertek.com/">Markertek</a></li>
</ul>
<p><em>Please be aware that the provided URL(s) is not a LaCie maintained or monitored website.</em></p>

---

Plain text:

```
Unfortunately, we do not sell or provide power supplies separately; however, here is a link that shows LaCie’s external drives’ power supply specifications
https://www.lacie.com/support/kb/identifying-my-power-supply-007350en/
and how to identify a bad power supply
https://www.lacie.com/support/kb/identify-a-bad-power-supply-006242en/.

The specifications of the required power supply for your device are the following:
Electrical specs:
Connector type:
Model number:

As long as it meets those specs the power supply will work on your hard drive. The drives were designed to use universal power supplies, so we do not have a specific brand or store we recommend for this. For your convenience, here are some websites where you can see about purchasing a replacement power supply:
-http://www.12vadapters.com/
-https://www.amazon.com/
-http://www.markertek.com/
Please be aware that the provided URL(s) is not a LaCie maintained or monitored website.

```
---

HTML code:

```
<p>Unfortunately, we do not sell or provide power supplies separately; however, here is a link that shows LaCie&rsquo;s external drives&rsquo; <a href="https://www.lacie.com/support/kb/identifying-my-power-supply-007350en/">power supply specifications</a> and <a href="https://www.lacie.com/support/kb/identify-a-bad-power-supply-006242en/">how to identify a bad power supply</a>.</p>
<p>The specifications of the required power supply for your device are the following:</p>
<ul>
<li>Electrical specs:</li>
<li>Connector type:</li>
<li>Model number:</li>
</ul>
<p>As long as it meets those specs the power supply will work on your hard drive. The drives were designed to use universal power supplies, so we do not have a specific brand or store we recommend for this. For your convenience, here are some websites where you can see about purchasing a replacement power supply:</p>
<ul>
<li><a href="http://www.12vadapters.com/">12VAdapters</a></li>
<li><a href="https://www.amazon.com/">Amazon</a></li>
<li><a href="http://www.markertek.com/">Markertek</a></li>
</ul>
<p><em>Please be aware that the provided URL(s) is not a LaCie maintained or monitored website.</em></p>
```
---

Speed note:

Composed an email to the customer providing help links for the power supply identification, including specifications of the power supply and places to find, with third party url script.
