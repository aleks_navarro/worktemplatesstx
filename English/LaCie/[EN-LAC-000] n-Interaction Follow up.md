### [EN-LAC-000] n-Interaction Follow up

Overview:

Start and ending for an email from the second interaction with the customer.

---

Preview:

<p>Hello {!Contact.FirstName} {!Contact.LastName}</p>
<p>This is {!Case.OwnerFirstName} from LaCie Support. Thank you for replying to our last email regarding \##issue##.</p>
<p>\##body##</p>
<p>We look forward to your prompt response.</p>
<p>Best Regards,<br />{!Case.OwnerFirstName}<br />LaCie Support</p>

---

Plain text:

```
Hello {!Contact.FirstName} {!Contact.LastName}

This is {!Case.OwnerFirstName} from LaCie Support. Thank you for replying to our last email regarding \##issue##.


We look forward to your prompt response.

Best Regards,
{!Case.OwnerFirstName}
LaCie Support
```
---

HTML code:

```
<p>Hello {!Contact.FirstName} {!Contact.LastName}</p>
<p>This is {!Case.OwnerFirstName} from LaCie Support. Thank you for replying to our last email regarding \##issue##.</p>
<p>\##body##</p>
<p>We look forward to your prompt response.</p>
<p>Best Regards,<br />{!Case.OwnerFirstName}<br />LaCie Support</p>
```
---

Speed note:

None specifically; "Composed an email to the customer...", "Emailed to the customer..." for example.
