### [EN-LAC-SRS] Data recovery information request

Overview:

Template for the request of information for the customer when a data recovery is offered.

---

Preview:

<p>Based on your feedback, your drive is not detected on your computer and the troubleshooting steps you attempted were not successful, Seagate has a possible solution that may be the best option for you, unfortunately appears that your drive may no longer be accessible by normal means. As such, LaCie works with Seagate in data recovery solutions, and Seagate would like to provide the following option to assist you with retrieving your data.<br />Seagate has a highly specialized Data Recovery team that can attempt an in-lab evaluation and retrieval of your drives data. I want to do all that I can to assist you and therefore Seagate would like to offer this option.<br />There are two important notes which you will need to know.</p>
<ol>
<li>The first one is that we can not guarantee a 100% recovery of your data. Our in-lab team will do an attempt to recover your irreplaceable data. Depending on the underlying issue, our recovery team will make their best effort to retrieve all of your data, however there is the possibility of some data loss and/or little to no recoverable data as a final result.</li>
<li>Due to the complexity of recovering data from hard drive components, we can not give you an accurate timeline on how long the service may take. You will be updated throughout the process but the recovery may take 25 to 30 days or possibly longer to complete.</li>
</ol>
<p>If these conditions are acceptable I would be happy to process the exception and get you<br />started with a data recovery case. We look forward to your response by verifying and filling the following information:</p>
<ul>
<li>Full name (first name and last name)</li>
<li>Phone number for contact</li>
<li>Valid email address</li>
<li>Name of the company (if applicable)</li>
<li>Address, city, postal code, state/Province</li>
<li>Is the address residential or commercial?</li>
<li>Full billing address (only if it&rsquo;s different from the shipping address)</li>
<li>What device needs recovery?</li>
<li>Serial number of device or hard drive?</li>
<li>What happened to your device?</li>
<li>Was your data encrypted?</li>
<li>Is a legal action involved?</li>
<li>What is the OS format would you like your data returned?</li>
<li>Which files are the highest, second and third priority? Any important File or Folder name?</li>
<li>Would you like to receive updates of the recovery via SMS?</li>
<li>Do you agree with terms and conditions (see below)?
<ul>
<li><a href="http://datarecovery.seagate.com/jobtrack/SRS_JobTrk_CCTermsAndConditions?lng=en-US">Seagate Data Recovery Services Terms and Conditions</a></li>
<li><a href="https://www.seagate.com/legal-privacy/privacy-policy/">Privacy Statement</a></li>
</ul>
</li>
</ul>

---

Plain text:

```
Based on your feedback, your drive is not detected on your computer and the troubleshooting steps you attempted were not successful, Seagate has a possible solution that may be the best option for you, unfortunately appears that your drive may no longer be accessible by normal means. As such, LaCie works with Seagate in data recovery solutions, and Seagate would like to provide the following option to assist you with retrieving your data.
Seagate has a highly specialized Data Recovery team that can attempt an in-lab evaluation and retrieval of your drives data. I want to do all that I can to assist you and therefore Seagate would like to offer this option.
There are (2) important notes which you will need to know.
1. The first one is that we can not guarantee a 100% recovery of your data. Our in-lab team will do an attempt to recover your irreplaceable data. Depending on the underlying issue, our recovery team will make their best effort to retrieve all of your data, however there is the possibility of some data loss and/or little to no recoverable data as a final result.
2. Due to the complexity of recovering data from hard drive components, we can not give you an accurate timeline on how long the service may take. You will be updated throughout the process but the recovery may take 25 to 30 days or possibly longer to complete.
If these conditions are acceptable I would be happy to process the exception and get you
started with a data recovery case. We look forward to your response by verifying and filling the following information:

Full name (first name and last name)
Phone number for contact
Valid email address
Name of the company (if applicable)
Address, city, postal code, state/Province
Is the address residential or commercial?
Full billing address (only if it’s different from the shipping address)
What device needs recovery?
Serial number of device or hard drive?
What happened to your device?
Was your data encrypted?
Is a legal action involved?
What is the OS format would you like your data returned?
Which files are the highest, second and third priority? Any important File or Folder name?
Would you like to receive updates of the recovery via SMS?
Do you agree with terms and conditions (see below)?
Seagate Data Recovery Services Terms and Conditions:
http://datarecovery.seagate.com/jobtrack/SRS_JobTrk_CCTermsAndConditions?lng=en-US
Privacy Statement:
https://www.seagate.com/legal-privacy/privacy-policy/.

```
---

HTML code:

```
<p>Based on your feedback, your drive is not detected on your computer and the troubleshooting steps you attempted were not successful, Seagate has a possible solution that may be the best option for you, unfortunately appears that your drive may no longer be accessible by normal means. As such, LaCie works with Seagate in data recovery solutions, and Seagate would like to provide the following option to assist you with retrieving your data.<br />Seagate has a highly specialized Data Recovery team that can attempt an in-lab evaluation and retrieval of your drives data. I want to do all that I can to assist you and therefore Seagate would like to offer this option.<br />There are two important notes which you will need to know.</p>
<ol>
<li>The first one is that we can not guarantee a 100% recovery of your data. Our in-lab team will do an attempt to recover your irreplaceable data. Depending on the underlying issue, our recovery team will make their best effort to retrieve all of your data, however there is the possibility of some data loss and/or little to no recoverable data as a final result.</li>
<li>Due to the complexity of recovering data from hard drive components, we can not give you an accurate timeline on how long the service may take. You will be updated throughout the process but the recovery may take 25 to 30 days or possibly longer to complete.</li>
</ol>
<p>If these conditions are acceptable I would be happy to process the exception and get you<br />started with a data recovery case. We look forward to your response by verifying and filling the following information:</p>
<ul>
<li>Full name (first name and last name)</li>
<li>Phone number for contact</li>
<li>Valid email address</li>
<li>Name of the company (if applicable)</li>
<li>Address, city, postal code, state/Province</li>
<li>Is the address residential or commercial?</li>
<li>Full billing address (only if it&rsquo;s different from the shipping address)</li>
<li>What device needs recovery?</li>
<li>Serial number of device or hard drive?</li>
<li>What happened to your device?</li>
<li>Was your data encrypted?</li>
<li>Is a legal action involved?</li>
<li>What is the OS format would you like your data returned?</li>
<li>Which files are the highest, second and third priority? Any important File or Folder name?</li>
<li>Would you like to receive updates of the recovery via SMS?</li>
<li>Do you agree with terms and conditions (see below)?
<ul>
<li><a href="http://datarecovery.seagate.com/jobtrack/SRS_JobTrk_CCTermsAndConditions?lng=en-US">Seagate Data Recovery Services Terms and Conditions</a></li>
<li><a href="https://www.seagate.com/legal-privacy/privacy-policy/">Privacy Statement</a></li>
</ul>
</li>
</ul>
```
---

Speed note:

Requesting to the customer the necessary information for the SRS case as delight. Including the basic statements for SRS and links to Seagate Recovery Servicec Terms and Conditions and Privacy Statement.
