### [EN-LAC-SRS] SRS contact

Overview:

SRS contact for LaCie.
**Currently deprecated.**

---

Preview:

<p>LaCie works with Seagate in data recovery solutions. For checking and reviewing advanced data recovery options please reply to this email or contact to Seagate Recovery Services (SRS phone number 1 800 475 0143).</p>

---

Plain text:

```
LaCie works with Seagate in data recovery solutions. For checking and reviewing advanced data recovery options please reply to this email or contact to Seagate Recovery Services (SRS phone number 1 800 475 0143).

```
---

HTML code:

```
<p>LaCie works with Seagate in data recovery solutions. For checking and reviewing advanced data recovery options please reply to this email or contact to Seagate Recovery Services (SRS phone number 1 800 475 0143).</p>
```
---

Speed note:

Invited to check data recover options, included SRS contact.
