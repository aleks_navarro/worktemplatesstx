### [EN-STX-000] 1-Interaction start

Overview:

Start and end of an email when a first contact is made.

---

Preview:

<p>Hello {!Contact.FirstName} {!Contact.LastName},</p>
<p>Welcome to Seagate Support, my name is {!Case.OwnerFirstName} and I'm glad to assist you today. We will work together to find a solution.</p>
<p>\##body##</p>
<p>We await for your reply with either this support was useful or more assistance is required.</p>
<p>Regards,<br />{!Case.OwnerFirstName}<br />Seagate Support</p>

---

Plain text:

```
Hello {!Contact.FirstName} {!Contact.LastName},

Welcome to Seagate Support, my name is {!Case.OwnerFirstName} and I'm glad to assist you today. We will work together to find a solution.

\##body##

We await for your reply with either this support was useful or more assistance is required.

Regards,
{!Case.OwnerFirstName}
Seagate Support
```
---

HTML code:

```
<p>Hello {!Contact.FirstName} {!Contact.LastName},</p>
<p>Welcome to Seagate Support, my name is {!Case.OwnerFirstName} and I'm glad to assist you today. We will work together to find a solution.</p>
<p>\##body##</p>
<p>We await for your reply with either this support was useful or more assistance is required.</p>
<p>Regards,<br />{!Case.OwnerFirstName}<br />Seagate Support</p>
```
---

Speed note:

None specifically; "Composed an email to the customer...", "Emailed to the customer..." for example.
