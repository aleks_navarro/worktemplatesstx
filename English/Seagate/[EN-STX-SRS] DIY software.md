### [EN-STX-SRS] DIY software

Overview:

Template when DIY recovery software is offered.

---

Preview:

<p>Based on your feedback, it unfortunately appears that your drive may no longer be accessible by normal means. As such, Seagate would like to provide the following option to assist you with retrieving your data.<br />Since your drive is detected by your computer, but not directly accessible, the Seagate Recovery Software solution is a great option for you. This specially designed application can perform a scan of your drive and search for any data which has been deleted and/or corrupted due to an improper operation.<br />Please note that the Seagate Data Recovery software success is conditional based on the drive and data status.</p>
<p>Download the software</p>
<ul>
<li><a href="http://www.seagate.com/files/www-content/services-software/seagate-recovery-services/recovery-software/_shared/masters/FRS-Premium-Win-x86-w11b-3.9.5.0.msi">For Windows</a></li>
<li><a href="http://www.seagate.com/files/www-content/services-software/seagate-recovery-services/recovery-software/_shared/masters/FRS-Premium-Mac-m11b-3.9.5.0.dmg">For macOS</a></li>
</ul>
<p>And introduce the following key during the installation:</p>
<p style="padding-left: 30px;">##keygen##.</p>
<p>We are going to try to run the Advance test, this will search for all the information that was deleted from your Seagate hard drive, when you finish the test DIY will show you all the files that it recovered and the files will be recovered for free to you.</p>
<p>For checking and reviewing advanced data recovery options please reply to this email.</p>

---

Plain text:

```
Based on your feedback, it unfortunately appears that your drive may no longer be accessible by normal means. As such, Seagate would like to provide the following option to assist you with retrieving your data.
Since your drive is detected by your computer, but not directly accessible, the Seagate Recovery Software solution is a great option for you. This specially designed application can perform a scan of your drive and search for any data which has been deleted and/or corrupted due to an improper operation.
Please note that the Seagate Data Recovery software success is conditional based on the drive and data status.

Download the software
For Windows
http://www.seagate.com/files/www-content/services-software/seagate-recovery-services/recovery-software/_shared/masters/FRS-Premium-Win-x86-w11b-3.9.5.0.msi
For macOS
http://www.seagate.com/files/www-content/services-software/seagate-recovery-services/recovery-software/_shared/masters/FRS-Premium-Mac-m11b-3.9.5.0.dmg
And introduce the following key during the installation:
##keygen##.
We are going to try to run the Advance test, this will search for all the information that was deleted from your Seagate hard drive, when you finish the test DIY will show you all the files that it recovered and the files will be recovered for free to you.

For checking and reviewing advanced data recovery options please reply to this email.

```
---

HTML code:

```
<p>Based on your feedback, it unfortunately appears that your drive may no longer be accessible by normal means. As such, Seagate would like to provide the following option to assist you with retrieving your data.<br />Since your drive is detected by your computer, but not directly accessible, the Seagate Recovery Software solution is a great option for you. This specially designed application can perform a scan of your drive and search for any data which has been deleted and/or corrupted due to an improper operation.<br />Please note that the Seagate Data Recovery software success is conditional based on the drive and data status.</p>
<p>Download the software</p>
<ul>
<li><a href="http://www.seagate.com/files/www-content/services-software/seagate-recovery-services/recovery-software/_shared/masters/FRS-Premium-Win-x86-w11b-3.9.5.0.msi">For Windows</a></li>
<li><a href="http://www.seagate.com/files/www-content/services-software/seagate-recovery-services/recovery-software/_shared/masters/FRS-Premium-Mac-m11b-3.9.5.0.dmg">For macOS</a></li>
</ul>
<p>And introduce the following key during the installation:</p>
<p style="padding-left: 30px;">##keygen##.</p>
<p>We are going to try to run the Advance test, this will search for all the information that was deleted from your Seagate hard drive, when you finish the test DIY will show you all the files that it recovered and the files will be recovered for free to you.</p>
<p>For checking and reviewing advanced data recovery options please reply to this email.</p>
```
---

Speed note:

2. Generated a promo code for DIY.
3. Composed an email to the customer providing DIY data recovery, with download links and key generated from server. Requested to reply emaill with results and check advanced recovery options if needed.
