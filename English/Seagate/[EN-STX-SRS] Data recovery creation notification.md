### [EN-STX-SRS] Data recovery creation notification

Overview:

explanation of template

---

Preview:

just paste the raw HTML code here, Markdown renders it automatically

---

Plain text:

```
Here is the information from the online Data Rescue Service that you should be aware:
Terms and conditions
http://datarecovery.seagate.com/jobtrack/SRS_JobTrk_CCTermsAndConditions?lng=en-us.
and Privacy statement
https://www.seagate.com/legal-privacy/privacy-policy/.
The SRS (Seagate Recovery Services) case number: ##cnssrs##.
The tracking number of the shipment label: ##pktrn##.

You are going to receive an email from the SRS team with instructions and a printable shipping label to send the hard drive. Please follow their instructions to start the process.
Remember to pack the drive safely, you can check a guide here
https://www.seagate.com/support/warranty-and-replacements/packing-and-shipping-instructions/.
Once you have sent the faulty device, SRS is going to receive it, and then they are going to start the data recovery process. Between one or two weeks later, you will receive another hard drive with the rescued information from the faulty drive stored in it.
If the hard drive is in warranty, SRS team will make a warranty replacement order for the faulty device. Depending on if the model of the drive is still in stock they will send another equal, if not they are going to send a recently model equivalent with the same capacity. You will be receiving this warranty replacement drive first, and then the hard drive with the rescued information.

From now on your case of data recovery and warranty will be taken by SRS, any doubt or question regarding this process will be solved by them. You can contact them by phone 1-800-475-0143 or replying to the email they will send you in the next 24 hours, give them the SRS case number (##cnssrs##) and they will gladly attend you.

This technical support case (number {!Case.CaseNumber}) will be closed since it is going to be taken by SRS from now. If you have another question or doubt regarding Technical Support or cannot reach communication with SRS, please reply to this email or open a new case for Technical Support.

Thank you for choosing ##cpname## for your digital storage needs.

```
---

HTML code:

```
here goes the HTML code
```
---

Speed note:

associated speed note for notes
