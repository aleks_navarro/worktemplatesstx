### [EN-STX-TSs] SeaTools Booteable

Overview:

Instructions to use SeaTools Booteable, use it for advanced cases with internal hard drives.

---

Preview:

<p>We are going to try to perform tests with SeaTools Bootable, which are a set of tools for performing tests over a drive with no need for an operating system installed. Here are the following steps to test the drive:</p>
<ol>
<li>Connect the hard drive to the computer, check that the cables are correctly connected and the drive goes on properly and is also recognized by the BIOS of the computer.<br />
<ul>
<li>To access the BIOS of the computer please check the documentation of the motherboard or computer manufacturer.</li>
</ul>
</li>
<li><a href="https://www.seagate.com/support/downloads/seatools/,">Download SeaTools Bootable</a> in this same link you can find full software documentation.</li>
<li>With a USB drive, we are going to create a <a href="https://www.seagate.com/support/kb/how-to-use-seatools-bootable-007843en/">Bootable drive</a>.</li>
<li>We are going to perform the following tests:<br />
<ul>
<li>S.M.A.R.T Check</li>
<li>Short Drive Self Test</li>
<li>Short Generic Test<br />Note: the tests may take their time, so please be patient.</li>
</ul>
</li>
</ol>
<p>For every test result from the troubleshooting, please take a photo and reply to this email attaching it. This is going to help us to get a diagnostics of the actual health of the drive and if it can be recovered in situ.<br /><em>Please be aware that testing with SeaTools Booteable could result in the inability to access your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.</em></p>

---

Plain text:

```
We are going to try to perform tests with SeaTools Bootable, which are a set of tools for performing tests over a drive with no need for an operating system installed. Here are the following steps to test the drive:
1. Connect the hard drive to the computer, check that the cables are correctly connected and the drive goes on properly and is also recognized by the BIOS of the computer.
To access the BIOS of the computer please check the documentation of the motherboard or computer manufacturer.
2. Download SeaTools Bootable
https://www.seagate.com/support/downloads/seatools/,
in this same link you can find full software documentation.
3. With a USB drive, we are going to create a Bootable drive
https://www.seagate.com/support/kb/how-to-use-seatools-bootable-007843en/.
4. We are going to perform the following tests:
S.M.A.R.T Check
Short Drive Self Test
Short Generic Test
Note: the tests may take their time, so please be patient.

For every test result from the troubleshooting, please take a photo and reply to this email attaching it. This is going to help us to get a diagnostics of the actual health of the drive and if it can be recovered in situ.
Please be aware that testing with SeaTools Booteable could result in the inability to access your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.

```
---

HTML code:

```
<p>We are going to try to perform tests with SeaTools Bootable, which are a set of tools for performing tests over a drive with no need for an operating system installed. Here are the following steps to test the drive:</p>
<ol>
<li>Connect the hard drive to the computer, check that the cables are correctly connected and the drive goes on properly and is also recognized by the BIOS of the computer.<br />
<ul>
<li>To access the BIOS of the computer please check the documentation of the motherboard or computer manufacturer.</li>
</ul>
</li>
<li><a href="https://www.seagate.com/support/downloads/seatools/,">Download SeaTools Bootable</a> in this same link you can find full software documentation.</li>
<li>With a USB drive, we are going to create a <a href="https://www.seagate.com/support/kb/how-to-use-seatools-bootable-007843en/">Bootable drive</a>.</li>
<li>We are going to perform the following tests:<br />
<ul>
<li>S.M.A.R.T Check</li>
<li>Short Drive Self Test</li>
<li>Short Generic Test<br />Note: the tests may take their time, so please be patient.</li>
</ul>
</li>
</ol>
<p>For every test result from the troubleshooting, please take a photo and reply to this email attaching it. This is going to help us to get a diagnostics of the actual health of the drive and if it can be recovered in situ.<br /><em>Please be aware that testing with SeaTools Booteable could result in the inability to access your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.</em></p>
```
---

Speed note:

Including instructions to use and test with SeaTools Booteable, with download and user manual links, requesting results and possible data loss script.
