### [EN-STX-TSs] SeaTools

Overview:

Basic information for using SeaTools and requesting results, including links to support page and user manual.

---

Preview:

<p>We&rsquo;re going to perform some tests to check the health of the drive.</p>
<ol>
<li><a href="https://www.seagate.com/support/downloads/seatools/.">Download SeaTools for Windows</a></li>
<li>In the same link, you can find more information and documentation about the software and how to use it.</li>
<li>Follow carefully the <a href="https://www.seagate.com/support/kb/seagate-usb-external-drive-diagnostics-006153en/#Seatools">instructions listed in the SeaTools Diagnostics</a> until step 4, where we&rsquo;re going to choose the option &ldquo;Short Drive Self Test &rdquo;. The software will perform a basic test and diagnostic about the health of the unit.</li>
</ol>
<p><br />Please reply to this email with the results of this test attached, preferably attaching a screenshot of your screen once the test is finished. This will determine if a warranty replacement is needed or if the drive just had errors that could be fixed with the test.<br /><em>Please be aware that testing with SeaTools could result in the inability to access your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.</em></p>

---

Plain text:

```
We’re going to perform some tests to check the health of the drive.
1. Download SeaTools for Windows
https://www.seagate.com/support/downloads/seatools/.
2. In the same link, you can find more information and documentation about the software and how to use it.
3. Follow carefully the instructions listed in the SeaTools Diagnostics:
https://www.seagate.com/support/kb/seagate-usb-external-drive-diagnostics-006153en/#Seatools
until step 4, where we’re going to choose the option “Short Drive Self Test ”. The software will perform a basic test and diagnostic about the health of the unit.
Please reply to this email with the results of this test attached, preferably attaching a screenshot of your screen once the test is finished. This will determine if a warranty replacement is needed or if the drive just had errors that could be fixed with the test.
Please be aware that testing with SeaTools could result in the inability to access your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.

```
---

HTML code:

```
<p>We&rsquo;re going to perform some tests to check the health of the drive.</p>
<ol>
<li><a href="https://www.seagate.com/support/downloads/seatools/.">Download SeaTools for Windows</a></li>
<li>In the same link, you can find more information and documentation about the software and how to use it.</li>
<li>Follow carefully the <a href="https://www.seagate.com/support/kb/seagate-usb-external-drive-diagnostics-006153en/#Seatools">instructions listed in the SeaTools Diagnostics</a> until step 4, where we&rsquo;re going to choose the option &ldquo;Short Drive Self Test &rdquo;. The software will perform a basic test and diagnostic about the health of the unit.</li>
</ol>
<p><br />Please reply to this email with the results of this test attached, preferably attaching a screenshot of your screen once the test is finished. This will determine if a warranty replacement is needed or if the drive just had errors that could be fixed with the test.<br /><em>Please be aware that testing with SeaTools could result in the inability to access your important data (pictures, videos, music and documents), which cannot be recovered without cost to you.</em></p>
```
---

Speed note:

Including links to SeaTools download link and user manual, requesting tests and results, with possible data loss script.
