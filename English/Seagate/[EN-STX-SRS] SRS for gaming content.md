### [EN-STX-SRS] SRS for gaming content

Overview:

Use this template when a customer requests to recover data from hard drives with gaming content, mainly from Xbox and PS.

---

Preview:

<p>Please keep in mind that the SRS Department can recover Screenshots, Game Progression, Extra material from the Games, Pics, Videos, Etc. "The game cannot be recovered due the copyrights and encryption, the customer needs to reinstall the game to be able to play again. The SRS department can attempt to make a mirror of the game but cannot be sure that the game will work once get reinstalled on the console.</p>

---

Plain text:

```
Please keep in mind that the SRS Department can recover Screenshots, Game Progression, Extra material from the Games, Pics, Videos, Etc. The game cannot be recovered due the copyrights and encryption, the customer needs to reinstall the game to be able to play again. The SRS department can attempt to make a mirror of the game but cannot be sure that the game will work once get reinstalled on the console.

```
---

HTML code:

```
<p>Please keep in mind that the SRS Department can recover Screenshots, Game Progression, Extra material from the Games, Pics, Videos, Etc. "The game cannot be recovered due the copyrights and encryption, the customer needs to reinstall the game to be able to play again. The SRS department can attempt to make a mirror of the game but cannot be sure that the game will work once get reinstalled on the console.</p>
```
---

Speed note:

Explaining to the customer that the gaming data cannot be recovered conventionally.
