### [EN-STX-GEN] Thank-You Email side note for enterprises

Overview:

Use this template as an extra for enterpreises.

---

Preview:

<p>As a final note, I would like to recommend you to check the solutions for enterprises provided by Seagate. More information in the following links:</p>
<ul>
<li><a href="https://www.seagate.com/enterprise-storage/">Enterprise Storage</a></li>
<li><a href="https://www.seagate.com/enterprise-storage/enterprise-security/">Enterprise Security</a></li>
<li><a href="https://labs.seagate.com/">Lyve Data Labs</a></li>
<li><a href="https://www.seagate.com/partners/">Seagate Partners</a>.</li>
</ul>

---

Plain text:

```
As a final note, I would like to recommend you to check the solutions for enterprises provided by Seagate. More information in the following links:
Enterprise Storage
https://www.seagate.com/enterprise-storage/
Enterprise Security
https://www.seagate.com/enterprise-storage/enterprise-security/
Lyve Data Labs
https://labs.seagate.com/
Seagate Partners
https://www.seagate.com/partners/.
```
---

HTML code:

```
<p>As a final note, I would like to recommend you to check the solutions for enterprises provided by Seagate. More information in the following links:</p>
<ul>
<li><a href="https://www.seagate.com/enterprise-storage/">Enterprise Storage</a></li>
<li><a href="https://www.seagate.com/enterprise-storage/enterprise-security/">Enterprise Security</a></li>
<li><a href="https://labs.seagate.com/">Lyve Data Labs</a></li>
<li><a href="https://www.seagate.com/partners/">Seagate Partners</a>.</li>
</ul>
```
---

Speed note:

Included links for entreprises options, storage, security, Lyve Data Labs and Seagate Parners.
