### [EN-STX-TSs] SeaChest

Overview:

Reccomendation for SeaChest, use only with advanced cases.

---

Preview:

<p>It is recommended to test the drive using SeaChest utility, which is a set of expert tools designed by Seagate for testing, mantaining and configuring drives. Works in Windows and Linux. More information in the <a href="https://www.seagate.com/support/software/seachest/">SeaChest Support Page</a>.</p>

---

Plain text:

```
It is recommended to test the drive using SeaChest utility, which is a set of expert tools designed by Seagate for testing, mantaining and configuring drives. Works in Windows and Linux. More information in the SeaChest Support Page:
https://www.seagate.com/support/software/seachest/.

```
---

HTML code:

```
<p>It is recommended to test the drive using SeaChest utility, which is a set of expert tools designed by Seagate for testing, mantaining and configuring drives. Works in Windows and Linux. More information in the <a href="https://www.seagate.com/support/software/seachest/">SeaChest Support Page</a>.</p>
```
---

Speed note:

Reccomended to use SeaChest for the hard drive, including link to support page.
