### [EN-STX-TSs] Not suitable drive for gaming

Overview:

Brief reccomendation and explanation of why the driver being used is not suitable for gaming content, including links to another reccomendations.

---

Preview:

<p>As a final note, I would like to point to the fact that the current drive that you were using is not prepared for gaming environments like Xbox or setting up gaming server. I recommend you to check the drives that are designed for gaming:</p>
<ul>
<li>For <a href="https://www.seagate.com/consumer/play/">Xbox and PS4</a>.</li>
<li>For <a href="https://www.seagate.com/internal-hard-drives/hdd/firecuda/">PC Gaming</a>.</li>
</ul>

---

Plain text:

```
As a final note, I would like to point to the fact that the current drive that you were using is not prepared for gaming environments like Xbox or setting up gaming server. I recommend you to check the drives that are designed for gaming:
For Xbox and PS4
https://www.seagate.com/consumer/play/.
For PC Gaming
https://www.seagate.com/internal-hard-drives/hdd/firecuda/.

```
---

HTML code:

```
<p>As a final note, I would like to point to the fact that the current drive that you were using is not prepared for gaming environments like Xbox or setting up gaming server. I recommend you to check the drives that are designed for gaming:</p>
<ul>
<li>For <a href="https://www.seagate.com/consumer/play/">Xbox and PS4</a>.</li>
<li>For <a href="https://www.seagate.com/internal-hard-drives/hdd/firecuda/">PC Gaming</a>.</li>
</ul>
```
---

Speed note:

Explaining why the used driver is not accurate for gaming environments and including links for reccomendations, drives for Xbox, PS4 and PC.
