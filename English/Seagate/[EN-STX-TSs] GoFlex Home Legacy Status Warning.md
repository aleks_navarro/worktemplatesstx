### [EN-STX-TSs] GoFlex Home Legacy Status Warning

Overview:

General explanation regarding the legacy/EOL status of the Seagate FreeAgent Goflex Home.

---

Preview:

<p>Please be aware that the device Seagate FreeAgent GoFlex Home and its associated software is deprecated and its support is only as legacy, any troubleshooting made to it is at your own risk. Do not reset the device or disconnect from power source since these operations will leave the device completely inoperable. For more please the <a href="https://www.seagate.com/support/external-hard-drives/network-storage/goflex-home/">support page for Seagate FreeAgent GoFlex Home</a>.</p>

---

Plain text:

```
Please be aware that the device Seagate FreeAgent GoFlex Home and its associated software is deprecated and its support is only as legacy, any troubleshooting made to it is at your own risk. Do not reset the device or disconnect from power source since these operations will leave the device completely inoperable. For more please the support page for Seagate FreeAgent GoFlex Home:
https://www.seagate.com/support/external-hard-drives/network-storage/goflex-home/.

```
---

HTML code:

```
<p>Please be aware that the device Seagate FreeAgent GoFlex Home and its associated software is deprecated and its support is only as legacy, any troubleshooting made to it is at your own risk. Do not reset the device or disconnect from power source since these operations will leave the device completely inoperable. For more please the <a href="https://www.seagate.com/support/external-hard-drives/network-storage/goflex-home/">support page for Seagate FreeAgent GoFlex Home</a>.</p>
```
---

Speed note:

Explaining the legacy status of the STX FreeAgent GoFlex Home.
