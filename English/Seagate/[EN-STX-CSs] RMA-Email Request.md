### [EN-STX-CSs] RMA-Email Request

Overview:

This template is used when the information for an RMA will be requested to the customer via email.

---

Preview:

<p>Analyzing your information it seems that your drive has symptoms of failure. Unusual noises (such as clicks, grinding, whining, etc) are the most common indicators of physical drive failure, especially if the drive has started malfunctioning or causing logical errors. Please keep your drive disconnected from the computer to prevent further damage to the disk or the information stored in it.</p>
<p>After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}. We will be happy to replace your drive and I'm sorry for all the trouble this may have caused.<br />However, we can make for this occasion a warranty extension for your drive to get replaced. Please be aware that this is a one-time extraordinary case in which Seagate can assist because we care about the satisfaction of our customers.</p>
<p>Important: Before proceeding with the warranty replacement order, be aware that data recovery is not covered under the warranty and by proceeding directly to the warranty replacement the data in the hard drive will be lost. Seagate works in data recovery. For checking and reviewing advanced data recovery options please reply to this email.</p>
<p>Otherwise, if data recovery is not necessary, We can place the order of warranty replacement for you, for this please verify and fill-in the following information:</p>
<ul>
<li>Full name (first name and last name)</li>
<li>Phone number for contact</li>
<li>Valid email address</li>
<li>Name of the company (if applicable)</li>
<li>Address, city, postal code, state/Province</li>
<li>Is the address residential or commercial?</li>
<li>Full billing address (only if it&rsquo;s different from the shipping address)</li>
<li>Would you like to receive updates via SMS?</li>
<li>Which kind of replacement plan you would like, Standard or Premium?</li>
<li>Do you agree with terms and conditions (see below)?</li>
</ul>
<p>Once the replacement request is finished, you will receive an email with instructions for the replacement. Please read them carefully and follow them.</p>
<p>For the replacement order you agree to:<br />Your continuance of the product return process means that you do agree to the Seagate privacy policy and Seagate online terms and conditions available at the legal page on our website at www.seagate.com, and to the return policy, shipping instructions, and E-Commerce &amp; RMA Terms available on the support pages of our website. You will be provided an order acknowledgment following submission of your order, giving you the ability to review the entirety of these online terms and contact Seagate if you have any questions, or cancel your order if you do not agree to these terms.<br />Your continuance of the product return process means that you do agree that Seagate products may not be exported to restricted countries, or used to develop, produce or distribute chemical, biological, or nuclear weapons, missile technologies, or other terrorist activities.<br />Please be advised, Seagate may replace your product with a product that was previously used, repaired, and tested to meet Seagate specifications.</p>
<p>For more information about warranty terms, conditions and statements please check <a href="https://www.seagate.com/support/warranty-and-replacements/">Seagate Warranty and Replacements</a>.</p>

---

Plain text:

```
Analyzing your information it seems that your drive has symptoms of failure. Unusual noises (such as clicks, grinding, whining, etc) are the most common indicators of physical drive failure, especially if the drive has started malfunctioning or causing logical errors. Please keep your drive disconnected from the computer to prevent further damage to the disk or the information stored in it.

After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}. We will be happy to replace your drive and I'm sorry for all the trouble this may have caused.
However, we can make for this occasion a warranty extension for your drive to get replaced. Please be aware that this is a one-time extraordinary case in which Seagate can assist because we care about the satisfaction of our customers.

Important: Before proceeding with the warranty replacement order, be aware that data recovery is not covered under the warranty and by proceeding directly to the warranty replacement the data in the hard drive will be lost. Seagate works in data recovery. For checking and reviewing advanced data recovery options please reply to this email.

Otherwise, if data recovery is not necessary, we can place the order of warranty replacement for you, for this please verify and fill-in the following information:

Full name (first name and last name)
Phone number for contact
Valid email address
Name of the company (if applicable)
Address, city, postal code, state/Province
Is the address residential or commercial?
Full billing address (only if it’s different from the shipping address)
Would you like to receive updates via SMS?
Which kind of replacement plan you would like, Standard or Premium?
Do you agree with terms and conditions (see below)?

Once the replacement request is finished, you will receive an email with instructions for the replacement. Please read them carefully and follow them.

For the replacement order you agree to:
Your continuance of the product return process means that you do agree to the Seagate privacy policy and Seagate online terms and conditions available at the legal page on our website at www.seagate.com, and to the return policy, shipping instructions, and E-Commerce & RMA Terms available on the support pages of our website. You will be provided an order acknowledgment following submission of your order, giving you the ability to review the entirety of these online terms and contact Seagate if you have any questions, or cancel your order if you do not agree to these terms.
Your continuance of the product return process means that you do agree that Seagate products may not be exported to restricted countries, or used to develop, produce or distribute chemical, biological, or nuclear weapons, missile technologies, or other terrorist activities.
Please be advised, Seagate may replace your product with a product that was previously used, repaired, and tested to meet Seagate specifications.

For more information about warranty terms, conditions and statements please check Seagate Warranty and Replacements:
https://www.seagate.com/support/warranty-and-replacements/.

```
---

HTML code:

```
<p>Analyzing your information it seems that your drive has symptoms of failure. Unusual noises (such as clicks, grinding, whining, etc) are the most common indicators of physical drive failure, especially if the drive has started malfunctioning or causing logical errors. Please keep your drive disconnected from the computer to prevent further damage to the disk or the information stored in it.</p>
<p>After reviewing serial number {!Case.Serial_Number__c}, it shows it is in warranty until {!Case.Warranty_End_Date__c}. We will be happy to replace your drive and I'm sorry for all the trouble this may have caused.<br />However, we can make for this occasion a warranty extension for your drive to get replaced. Please be aware that this is a one-time extraordinary case in which Seagate can assist because we care about the satisfaction of our customers.</p>
<p>Important: Before proceeding with the warranty replacement order, be aware that data recovery is not covered under the warranty and by proceeding directly to the warranty replacement the data in the hard drive will be lost. Seagate works in data recovery. For checking and reviewing advanced data recovery options please reply to this email.</p>
<p>Otherwise, if data recovery is not necessary, we can place the order of warranty replacement for you, for this please verify and fill-in the following information:</p>
<ul>
<li>Full name (first name and last name)</li>
<li>Phone number for contact</li>
<li>Valid email address</li>
<li>Name of the company (if applicable)</li>
<li>Address, city, postal code, state/Province</li>
<li>Is the address residential or commercial?</li>
<li>Full billing address (only if it&rsquo;s different from the shipping address)</li>
<li>Would you like to receive updates via SMS?</li>
<li>Which kind of replacement plan you would like, Standard or Premium?</li>
<li>Do you agree with terms and conditions (see below)?</li>
</ul>
<p>Once the replacement request is finished, you will receive an email with instructions for the replacement. Please read them carefully and follow them.</p>
<p>For the replacement order you agree to:<br />Your continuance of the product return process means that you do agree to the Seagate privacy policy and Seagate online terms and conditions available at the legal page on our website at www.seagate.com, and to the return policy, shipping instructions, and E-Commerce &amp; RMA Terms available on the support pages of our website. You will be provided an order acknowledgment following submission of your order, giving you the ability to review the entirety of these online terms and contact Seagate if you have any questions, or cancel your order if you do not agree to these terms.<br />Your continuance of the product return process means that you do agree that Seagate products may not be exported to restricted countries, or used to develop, produce or distribute chemical, biological, or nuclear weapons, missile technologies, or other terrorist activities.<br />Please be advised, Seagate may replace your product with a product that was previously used, repaired, and tested to meet Seagate specifications.</p>
<p>For more information about warranty terms, conditions and statements please check <a href="https://www.seagate.com/support/warranty-and-replacements/">Seagate Warranty and Replacements</a>.</p>
```
---

Speed note:

Explaining the hard drive failure and reviewing the current warranty status of the drive. Requesting information for RMA and including ECOMM1, ITA and link to Seagate Warranty and Replacements.
