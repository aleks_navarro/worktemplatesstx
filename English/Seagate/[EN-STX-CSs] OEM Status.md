### [EN-STX-CSs] OEM Status

Overview:

When the hard drive of the customer is marked as OEM by the system.

---

Preview:

<p>Our records show this drive, ##drivename## with serial number {!Case.Serial_Number__c}, is an OEM drive, which generally means that either it was a component of a non-Seagate system, or it was sold to a company with the intent of being used as such. <br />OEM companies will usually purchase the warranty along with the drive, which means that it is no longer honored by Seagate, but by the company that purchased it. I recommend contacting the place the drive was purchased from or the manufacturer of the system it came out of.<br />If you think this information is wrong, please reply to this email attaching the invoice note or proof of purchase of your hard drive, and we will analyze the warranty situation of the drive.</p>
<p>For more information outlining OEM products please <a href="https://www.seagate.com/support/kb/warranty-guidelines-for-oem-disk-drive-products-169851en/">check this support page</a>.</p>

---

Plain text:

```
Our records show this drive, ##drivename## with serial number {!Case.Serial_Number__c}, is an OEM drive, which generally means that either it was a component of a non-Seagate system, or it was sold to a company with the intent of being used as such. OEM companies will usually purchase the warranty along with the drive, which means that it is no longer honored by Seagate, but by the company that purchased it. I recommend contacting the place the drive was purchased from or the manufacturer of the system it came out of.
If you think this information is wrong, please reply to this email attaching the invoice note or proof of purchase of your hard drive, and we will analyze the warranty situation of the drive.

For more information outlining OEM products please check this support page
https://www.seagate.com/support/kb/warranty-guidelines-for-oem-disk-drive-products-169851en/.

```
---

HTML code:

```
<p>Our records show this drive, ##drivename## with serial number {!Case.Serial_Number__c}, is an OEM drive, which generally means that either it was a component of a non-Seagate system, or it was sold to a company with the intent of being used as such. <br />OEM companies will usually purchase the warranty along with the drive, which means that it is no longer honored by Seagate, but by the company that purchased it. I recommend contacting the place the drive was purchased from or the manufacturer of the system it came out of.<br />If you think this information is wrong, please reply to this email attaching the invoice note or proof of purchase of your hard drive, and we will analyze the warranty situation of the drive.</p>
<p>For more information outlining OEM products please <a href="https://www.seagate.com/support/kb/warranty-guidelines-for-oem-disk-drive-products-169851en/">check this support page</a>.</p>
```
---

Speed note:

Explained to the customer that the hard drive is an OEM drive and suggesting to get in contact with the reseller for assistance. Requesting POP for more information about drive origin and including a help link explaining more about OEM drives.
